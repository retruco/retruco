# Retruco UI

## _Web user interface to bring out shared positions from argumented statements_

**_Retruco_** is a software fostering argumentative discussion around statements and allowing to bring out shared positions.

**_Retruco_** means _immediate, precise and firm response_ in spanish.

The main repository for this project is hosted on [FramaGit](https://framagit.org/): https://framagit.org/retruco/retruco.

## Install

```bash
cd ui/
```

Create a `.env` file to set configuration variables (you can use `example.env` as a template). Then

```bash
npm install
```

## Server Launch

In development mode:

```bash
npm run dev
```

In production mode:

```bash
npm run build
npm start
```
