import bodyParser from "body-parser"
import compression from "compression"
import polka from "polka"
import sirv from "sirv"

import * as sapper from "@sapper/server"

import "@fortawesome/fontawesome-free/css/svg-with-js.css" // used for `fa-spin` class
import "material-icons/iconfont/material-icons.css"
import "typeface-roboto"

import "./styles/index.css"

const { PORT, NODE_ENV } = process.env
const dev = NODE_ENV === "development"

const app = polka()
if (dev) {
  // Add logging.
  const morgan = require("morgan")
  app.use(morgan("dev"))
}
app
  .use(
    compression({ threshold: 0 }),
    sirv("static", { dev }),
    bodyParser.json({
      limit: "1mb",
    }),
    // bodyParser.urlencoded({
    //   extended: false,
    //   type: "application/x-www-form-urlencoded",
    // }),
    sapper.middleware(),
  )
  .listen(PORT, error => {
    if (error) {
      console.log(`Error when calling listen on port ${PORT}:`, error)
    }
  })
