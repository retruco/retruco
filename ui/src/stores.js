import { writable } from "svelte/store"

import {generateBundles, Localization} from "./locales"

export const language = writable(null)
export const localize = writable((id, args, fallback) => fallback || id)

language.subscribe((newLanguage) => {
  const localization = new Localization(generateBundles([newLanguage]))
  localize.set(localization.getString.bind(localization))
})

export const breakpoint = writable("")
export const elevation = writable(false)
export const persistent = writable(true)
export const right = writable(false)
export const showNav = writable(true)
export const showNavMobile = writable(false)
