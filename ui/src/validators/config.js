import {
  validateChain,
  validateFunction,
  validateNonEmptyTrimmedString,
  validateUrl,
} from "@biryani/core"

export function validateApi(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of ["url"]) {
    remainingKeys.delete(key)
    const [value, error] = validateChain(
      validateUrl,
      validateFunction(url => url.trim("/") + "/"),
    )(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

export function validateConfig(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  {
    const key = "api"
    remainingKeys.delete(key)
    const [value, error] = validateApi(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of ["url"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
