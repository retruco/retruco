export { acceptedLanguages } from "@retruco/core/lib/locales"
import { CachedSyncIterable } from "cached-iterable"
import { FluentBundle } from "fluent"
import { mapBundleSync } from "fluent-sequence"
import { negotiateLanguages } from "fluent-langneg"

const localizedLanguages = ["en-US", "fr-FR"]
export const proposedLanguagesAndLabels = [
  ["en", "English"],
  ["fr", "Français"],
]

const bundleByLanguage = {}
const messagesByLanguage = {
  "en-US": `
About = About
About_site = About this site
Ask_questions_forever = Ask questions again and again…
BooleanField = Boolean
CardIdField = Card
Client_Token = Client Token
Clients = Clients
Create = Create
Creation_failed = Creation failed:
False = False
Field_Type = Field Type
Home = Home
ImageField = Image
InputEmailField = Email
InputNumberField = Number
InputUrlField = Link (URL)
Language = Language
New_Proposal = New Proposal
No = No
OK = OK
Proposals = Proposals
Retruco = Retruco
See_all_knowledge = See all knowledge
See_survey_on_one_page = See survey on one page
Select = Select
Sign_In = Sign In
Skip = Skip
Start = Start
Survey = Survey
Surveys = Surveys
Text = Text
TextField = Text
True = True
Username = Username
User_Surveys = Main Surveys Filled by User
Yes = Yes
  `,
  "fr-FR": `
About = À propos
About_site = À propos de ce site
Ask_questions_forever = Pose des questions en permanence…
BooleanField = Booléen
CardIdField = Fiche
Client_Token = Jeton du client
Clients = Clients
Create = Créer
Creation_failed = La création a échoué :
False = Faux
Field_Type = Type de champ
Home = Accueil
ImageField = Image
InputEmailField = Courriel
InputNumberField = Nombre
InputUrlField = Lien (URL)
Language = Langue
New_Proposal = Nouvelle proposition
No = Non
OK = OK
Proposals = Propositions
Retruco = Retruco
See_all_knowledge = Voir toutes les connaissances
See_survey_on_one_page = Voir le questionnaire sur une seule page
Select = Choisir
Sign_In = Connexion
Skip = Passer
Start = Démarrer
Survey = Questionnaire
Surveys = Questionnaires
Text = Texte
TextField = Texte
True = Vrai
Username = Nom d'utilisateur
User_Surveys = Questionnaires principaux remplis par l'utilisateur
Yes = Oui
  `,
}

/*
 * `Localization` handles translation formatting and fallback.
 *
 * The current negotiated fallback chain of languages is stored in the
 * `Localization` instance in form of an iterable of `FluentBundle`
 * instances.  This iterable is used to find the best existing translation for
 * a given identifier.
 *
 * Code taken from: https://github.com/projectfluent/fluent.js/blob/master/fluent-react/src/localization.js
 */
export class Localization {
  constructor(bundles) {
    this.bundles = CachedSyncIterable.from(bundles)
  }

  getBundle(id) {
    return mapBundleSync(this.bundles, id)
  }

  /// Find a translation by `id` and format it to a string using `args`.
  getString(id, args, fallback) {
    const bundle = this.getBundle(id)

    if (bundle === null) {
      return fallback || id
    }

    const msg = bundle.getMessage(id)
    return bundle.format(msg, args)
  }
}

export function* generateBundles(userLanguages) {
  // Choose locales that are best for the user.
  const currentLanguages = negotiateLanguages(
    userLanguages,
    localizedLanguages,
    {
      defaultLocale: "en",
    },
  )

  for (const language of currentLanguages) {
    yield bundleByLanguage[language]
  }
}

for (const language of localizedLanguages) {
  const bundle = new FluentBundle(language)
  const errors = bundle.addMessages(messagesByLanguage[language])
  if (errors.length > 0) {
    console.warn(
      `Errors occurred when adding messages for "${language}" language:\n${JSON.stringify(
        errors,
        null,
        2,
      )}`,
    )
  }
  bundleByLanguage[language] = bundle
}
