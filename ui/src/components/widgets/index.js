import AutocompleteOrCreate from "./AutocompleteOrCreate.svelte"
import Confirm from "./Confirm.svelte"
import InputDate from "./InputDate.svelte"
import InputEmail from "./InputEmail.svelte"
import InputPassword from "./InputPassword.svelte"
import InputText from "./InputText.svelte"
import Knowledge from "./Knowledge.svelte"
import RadioButtons from "./RadioButtons.svelte"
import YesNoButtons from "./YesNoButtons.svelte"

export default {
  AutocompleteOrCreate,
  Confirm,
  InputDate,
  InputEmail,
  InputPassword,
  InputText,
  Knowledge,
  RadioButtons,
  YesNoButtons,
}
