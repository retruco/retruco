# Retruco Core

## _Core library bring out shared positions from argumented statements_

Retruco is a software fostering argumentative discussion around statements and allowing to bring out shared positions.

**_Retruco_** means _immediate, precise and firm response_ in spanish.

The main repository for this project is hosted on [FramaGit](https://framagit.org/): https://framagit.org/retruco/retruco.
