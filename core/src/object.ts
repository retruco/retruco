export enum ObjectType {
  Card = "Card",
  Property = "Property",
  User = "User",
  Value = "Value",
}

export class ObjectCore {
  qualities?: { [languageId: string]: string[] | string }
  subTypes?: string[]
  tags?: string[]
  usages?: string[]

  constructor(
    public id: number,
    public createdAt: Date,
    public type: ObjectType,
    qualities?: { [languageId: string]: string[] | string } | null,
    subTypes?: string[] | null,
    tags?: string[] | null,
    usages?: string[] | null,
  ) {
    if (qualities !== undefined && qualities !== null) {
      this.qualities = qualities
    }
    if (subTypes !== undefined && subTypes !== null) {
      this.subTypes = subTypes
    }
    if (tags !== undefined && tags !== null) {
      this.tags = tags
    }
    if (usages !== undefined && usages !== null) {
      this.usages = usages
    }
  }
  static fromEntry(entry: ObjectEntry | null): ObjectCore | null {
    return entry === null
      ? null
      : new ObjectCore(
          entry.id,
          entry.created_at,
          entry.type,
          entry.qualities,
          entry.sub_types,
          entry.tags,
          entry.usages,
        )
  }
}

export interface ObjectEntry {
  created_at: Date
  id: number
  qualities: { [languageId: string]: string[] | string } | null
  sub_types: string[] | null
  tags?: string[]
  type: ObjectType
  usages?: string[]
}
