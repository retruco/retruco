export class Session {
  public clientSegment?: string

  constructor(
    public token: string,
    clientSegment: string | undefined | null,
    public expires: Date,
    public language: string,
    public userId: number,
  ) {
    if (clientSegment !== undefined && clientSegment !== null) {
      this.clientSegment = clientSegment
    }
  }
  static fromEntry(entry: SessionEntry | null): Session | null {
    return entry === null
      ? null
      : new Session(
          entry.token,
          entry.client_segment,
          entry.expires,
          entry.language,
          Number(entry.user_id),
        )
  }
}

export interface SessionEntry {
  token: string
  client_segment: string | null
  expires: Date
  language: string
  user_id: string
}
