import originalSlugify from "slug"

const slugifyCharmap = {
  ...originalSlugify.defaults.charmap,
  "'": " ",
  "@": " ",
  ".": " ",
}

interface Options {
  charmap: { [c: string]: string }
  mode: string
  replacement?: string
}

export function slugify(string: string, replacement?: string): string {
  const options: Options = {
    charmap: slugifyCharmap,
    mode: "rfc3986",
  }
  if (replacement) {
    options.replacement = replacement
  }
  return originalSlugify(string, options)
}
