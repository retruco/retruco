export class ClientUser {
  constructor(
    public clientSegment: string,
    public userId: number,
    public username: string,
  ) {}
  static fromEntry(entry: ClientUserEntry | null): ClientUser | null {
    return entry === null
      ? null
      : new ClientUser(entry.client_segment, entry.user_id, entry.username)
  }
}

export interface ClientUserEntry {
  client_segment: string
  user_id: number
  username: string
}
