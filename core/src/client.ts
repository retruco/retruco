import { validateNonEmptyTrimmedString } from "@biryani/core"

export class Client {
  public token?: string

  constructor(public segment: string, public name: string) {}

  static fromEntry(entry: ClientEntry | null): Client | null {
    if (entry === null) {
      return null
    }
    const newClient = new Client(entry.segment, entry.name)
    newClient.token = entry.token
    return newClient
  }

  static validateJson(data: any): [any, any] {
    if (data === null || data === undefined) {
      return [data, "Missing value"]
    }
    if (typeof data !== "object") {
      return [data, `Expected an object got "${typeof data}"`]
    }

    data = { ...data }
    const errors: { [key: string]: any } = {}
    const remainingKeys = new Set(Object.keys(data))

    for (let key of ["name", "segment"]) {
      remainingKeys.delete(key)
      const [value, error] = validateNonEmptyTrimmedString(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    for (let key of remainingKeys) {
      errors[key] = "Unexpected entry"
    }
    return Object.keys(errors).length === 0
      ? [new Client(data.segment, data.name), null]
      : [data, errors]
  }

  toJson(): ClientJson {
    return {
      name: this.name,
      segment: this.segment,
    }
  }
}

export interface ClientEntry {
  name: string
  segment: string
  token: string
}

export interface ClientJson {
  name: string
  segment: string
}
