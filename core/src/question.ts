import {
  validateBoolean,
  validateInteger,
  validateMaybeTrimmedString,
  validateMissing,
  validateNonEmptyTrimmedString,
  validateOption,
} from "@biryani/core"

import { Answer } from "./answer"

export class Question {
  static classByName: { [className: string]: typeof Question } = {}

  error?: string
  name?: string
  required: boolean = false

  constructor(
    public path: string,
    public label: string,
    { error = null, name = null, required = false }: QuestionOptions = {},
  ) {
    if (error !== null) {
      this.error = error
    }
    if (name !== null) {
      this.name = name
    }
    if (required) {
      this.required = true
    }
  }

  static fromValidJson(data: QuestionJson): Question {
    return Question.classByName[data.className].fromValidJsonContent(data)
  }

  static fromValidJsonContent(data: QuestionJson): Question {
    return new this(data.path, data.label, {
      error: data.error,
      name: data.name,
      required: data.required,
    })
  }

  static register(subClass: any) {
    Question.classByName[subClass.name] = subClass
  }

  static validateJson(data: any): [any, any] {
    if (data === null || data === undefined) {
      return [data, "Missing value"]
    }
    if (typeof data !== "object") {
      return [data, `Expected an object got "${typeof data}"`]
    }

    data = { ...data }
    const errors: { [key: string]: any } = {}
    const remainingKeys = new Set(Object.keys(data))

    remainingKeys.delete("className")
    const [className, classNameError] = validateNonEmptyTrimmedString(data.className)
    data.className = className
    let questionClass: any
    if (classNameError === null) {
      questionClass = Question.classByName[className]
      if (questionClass === undefined) {
        errors.className = "Unkwnown question class name"
      } else {
        data = questionClass.validateJsonContent(data, errors, remainingKeys)
      }
    } else {
      errors.className = classNameError
    }

    for (let key of remainingKeys) {
      errors[key] = "Unexpected entry"
    }
    return Object.keys(errors).length === 0
      ? [questionClass!.fromValidJsonContent(data), null]
      : [data, errors]
  }

  static validateJsonContent(
    data: any,
    errors: { [key: string]: any },
    remainingKeys: Set<string>,
  ) {
    for (let key of ["error", "name"]) {
      remainingKeys.delete(key)
      const [value, error] = validateMaybeTrimmedString(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    // TODO: Check that `path` is a valid JSON path.
    for (let key of ["label", "path"]) {
      remainingKeys.delete(key)
      const [value, error] = validateNonEmptyTrimmedString(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    {
      const key = "required"
      remainingKeys.delete(key)
      const [value, error] = validateOption(validateMissing, validateBoolean)(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    return data
  }

  toJson(): QuestionJson {
    const json: QuestionJson = {
      className: this.constructor.name,
      label: this.label,
      path: this.path,
    }
    if (this.error !== undefined) {
      json.error = this.error
    }
    if (this.name !== undefined) {
      json.name = this.name
    }
    if (this.required) {
      json.required = this.required
    }
    return json
  }

  validateAnswerJson(data: any): [any, any] {
    if (data === null || data === undefined) {
      return [data, "Missing value"]
    }
    if (typeof data !== "object") {
      return [data, `Expected an object got "${typeof data}"`]
    }

    data = { ...data }
    const errors: { [key: string]: any } = {}
    const remainingKeys = new Set(Object.keys(data))

    for (let key of ["external"]) {
      remainingKeys.delete(key)
      const [value, error] = validateBoolean(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    for (let key of ["questionPath", "updated"]) {
      remainingKeys.delete(key)
      const [value, error] = validateNonEmptyTrimmedString(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    for (let key of ["userId"]) {
      remainingKeys.delete(key)
      const [value, error] = validateInteger(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    for (let key of ["value"]) {
      remainingKeys.delete(key)
      const [value, error] = (this.required
        ? this.validateAnswerValueJson.bind(this)
        : validateOption(validateMissing, this.validateAnswerValueJson.bind(this)))(
        data[key],
      )
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    for (let key of remainingKeys) {
      errors[key] = "Unexpected entry"
    }
    return Object.keys(errors).length === 0
      ? [
          new Answer(
            data.external,
            data.questionPath,
            new Date(data.updated),
            data.userId,
            data.value,
          ),
          null,
        ]
      : [data, errors]
  }

  validateAnswerValueJson(data: any): [any, any] {
    return [
      data,
      `Class ${this.constructor.name} doesn't know how to validate the JSON of an answer value`,
    ]
  }
}

export interface QuestionJson {
  className: string
  error?: string | null
  label: string
  name?: string | null
  path: string
  required?: boolean
}

export type QuestionOptions = {
  error?: string | null
  name?: string | null
  required?: boolean | null
}

export class BooleanQuestion extends Question {
  static widgetSymbol = "YesNoButtons"

  validateAnswerValueJson(data: any): [any, any] {
    return validateBoolean(data)
  }
}
Question.register(BooleanQuestion)

export class ConfirmQuestion extends Question {
  static widgetSymbol = "Confirm"

  validateAnswerValueJson(_data: any): [any, any] {
    return [null, null]
  }
}
Question.register(ConfirmQuestion)

export class DateQuestion extends Question {
  static widgetSymbol = "InputDate"

  validateAnswerValueJson(data: any): [any, any] {
    return validateNonEmptyTrimmedString(data)
  }
}
Question.register(DateQuestion)

export class EmailQuestion extends Question {
  static widgetSymbol = "InputEmail"

  validateAnswerValueJson(data: any): [any, any] {
    return validateNonEmptyTrimmedString(data)
  }
}
Question.register(EmailQuestion)

export class PasswordQuestion extends Question {
  static widgetSymbol = "InputPassword"

  validateAnswerValueJson(data: any): [any, any] {
    return validateNonEmptyTrimmedString(data)
  }
}
Question.register(PasswordQuestion)

export class TextQuestion extends Question {
  static widgetSymbol = "InputText"

  validateAnswerValueJson(data: any): [any, any] {
    return validateNonEmptyTrimmedString(data)
  }
}
Question.register(TextQuestion)
