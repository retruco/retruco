import { validateNonEmptyTrimmedString, validateNumber } from "@biryani/core"

import { Question, QuestionJson, QuestionOptions } from "../question"

export class AutocompleteOrCreate extends Question {
  static widgetSymbol = "AutocompleteOrCreate"

  constructor(
    path: string,
    label: string,
    public domain: string,
    options: QuestionOptions = {},
  ) {
    super(path, label, options)
  }

  static fromValidJsonContent(data: AutocompleteOrCreateJson): AutocompleteOrCreate {
    return new this(data.path, data.label, data.domain, {
      error: data.error,
      name: data.name,
      required: data.required,
    })
  }

  static validateJsonContent(
    data: any,
    errors: { [key: string]: any },
    remainingKeys: Set<string>,
  ) {
    data = super.validateJsonContent(data, errors, remainingKeys)

    {
      const key = "domain"
      remainingKeys.delete(key)
      const [value, error] = validateNonEmptyTrimmedString(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    return data
  }

  toJson(): AutocompleteOrCreateJson {
    return {
      ...super.toJson(),
      domain: this.domain,
    }
  }

  validateAnswerValueJson(value: any): [any, any] {
    return validateNumber(value)
  }
}
Question.register(AutocompleteOrCreate)

interface AutocompleteOrCreateJson extends QuestionJson {
  domain: string
}
