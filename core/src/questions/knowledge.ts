import { validateBoolean } from "@biryani/core"

import { Autocompletion, AutocompletionJson } from "../autocompletion"
import { Question, QuestionJson, QuestionOptions } from "../question"

export class Knowledge extends Question {
  static widgetSymbol = "Knowledge"

  constructor(
    path: string,
    label: string,
    public autocompletion: Autocompletion,
    options: QuestionOptions = {},
  ) {
    super(path, label, options)
  }

  static fromValidJsonContent(data: KnowledgeJson): Knowledge {
    return new this(
      data.path,
      data.label,
      Autocompletion.fromValidJson(data.autocompletion),
      { error: data.error, name: data.name, required: data.required },
    )
  }

  static validateJsonContent(
    data: any,
    errors: { [key: string]: any },
    remainingKeys: Set<string>,
  ) {
    data = super.validateJsonContent(data, errors, remainingKeys)

    {
      const key = "autocompletion"
      remainingKeys.delete(key)
      const [value, error] = Autocompletion.validateJson(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    return data
  }

  toJson(): KnowledgeJson {
    return {
      ...super.toJson(),
      autocompletion: this.autocompletion.toJson(),
    }
  }

  validateAnswerValueJson(data: any): [any, any] {
    return validateBoolean(data)
  }
}
Question.register(Knowledge)

interface KnowledgeJson extends QuestionJson {
  autocompletion: AutocompletionJson
}
