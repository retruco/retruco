import {
  validateArray,
  validateChoice,
  validateNonEmptyTrimmedString,
} from "@biryani/core"

import { Question, QuestionJson, QuestionOptions } from "../question"

export interface ChoiceItem {
  label: string
  value: string
}

export class SingleChoice extends Question {
  static widgetSymbol = "RadioButtons"

  choices: ChoiceItem[]

  constructor(
    path: string,
    label: string,
    choices: (ChoiceItem | string)[],
    options: QuestionOptions = {},
  ) {
    super(path, label, options)
    this.choices = choices.map(choice =>
      typeof choice === "string" ? { label: choice, value: choice } : choice,
    )
  }

  static fromValidJsonContent(data: SingleChoiceJson): SingleChoice {
    return new this(data.path, data.label, data.choices, {
      error: data.error,
      name: data.name,
      required: data.required,
    })
  }

  static validateJsonContent(
    data: any,
    errors: { [key: string]: any },
    remainingKeys: Set<string>,
  ) {
    data = super.validateJsonContent(data, errors, remainingKeys)

    {
      const key = "choices"
      remainingKeys.delete(key)
      const [value, error] = validateArray(validateChoiceItem)(data[key])
      data[key] = value
      if (error !== null) {
        errors[key] = error
      }
    }

    return data
  }

  toJson(): SingleChoiceJson {
    return {
      ...super.toJson(),
      choices: this.choices,
    }
  }

  validateAnswerValueJson(value: any): [any, any] {
    return validateChoice(this.choices.map(choice => choice.value))(value)
  }
}
Question.register(SingleChoice)

interface SingleChoiceJson extends QuestionJson {
  choices: ChoiceItem[]
}

function validateChoiceItem(data: any): [any, any] {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors: { [key: string]: any } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of ["label", "value"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
