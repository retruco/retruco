export class Answer {
  constructor(
    public external: boolean,
    public questionPath: string,
    public updated: Date,
    public userId: number,
    public value: any,
  ) {}

  static fromEntry(entry: AnswerEntry | null): Answer | null {
    return entry === null
      ? null
      : new Answer(
          entry.external,
          entry.question_path,
          entry.updated,
          Number(entry.user_id),
          entry.value,
        )
  }

  toJson() {
    return {
      external: this.external,
      questionPath: this.questionPath,
      updated: this.updated.toISOString(),
      userId: this.userId,
      value: this.value,
    }
  }
}

export interface AnswerEntry {
  external: boolean
  question_path: string
  updated: Date
  user_id: string
  value: any
}

export interface AnswerJson {
  external: boolean
  questionPath: string
  updated: string
  userId: number
  value: any
}
