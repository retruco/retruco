import { Answer } from "./answer"
import { Question } from "./question"

export class Step {
  question?: Question
  state?: any

  constructor(
    question: Question | null | undefined,
    state: any,
    public answered: boolean = false,
    public answer?: Answer,
  ) {
    if (question !== undefined && question !== null) {
      this.question = question
    }
    if (state !== undefined && state !== null) {
      this.state = state
    }
  }
}
