import { ObjectCore, ObjectEntry, ObjectType } from "./object"

export class User extends ObjectCore {
  public isAdmin: boolean

  constructor(
    id: number,
    createdAt: Date,
    public type: ObjectType,
    qualities?: { [languageId: string]: string[] | string } | null,
    subTypes?: string[] | null,
    tags?: string[] | null,
    usages?: string[] | null,
    isAdmin?: boolean,
  ) {
    super(id, createdAt, type, qualities, subTypes, tags, usages)
    this.isAdmin = isAdmin === undefined ? false : isAdmin
  }
  static fromObjectAndUserEntries(
    objectEntry: ObjectEntry,
    userEntry: UserEntry | null,
  ): User | null {
    return userEntry === null
      ? null
      : new User(
          objectEntry.id,
          objectEntry.created_at,
          objectEntry.type,
          objectEntry.qualities,
          objectEntry.sub_types,
          objectEntry.tags,
          objectEntry.usages,
          userEntry.is_admin,
        )
  }
}

export interface UserEntry {
  id: number
  is_admin: boolean
}
