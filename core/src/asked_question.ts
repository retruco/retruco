import { Question, QuestionJson } from "./question"

export class AskedQuestion {
  constructor(
    public id: number,
    public answered: boolean,
    public question: Question,
    public state: any,
    public surveySegment: string,
    public userId: number,
  ) {}
  static fromEntry(entry: AskedQuestionEntry | null): AskedQuestion | null {
    return entry === null
      ? null
      : new AskedQuestion(
          Number(entry.id),
          entry.answered,
          Question.fromValidJson(entry.question_json),
          entry.state,
          entry.survey_segment,
          Number(entry.user_id),
        )
  }
}

export interface AskedQuestionEntry {
  id: string
  answered: boolean
  question_json: QuestionJson
  state: any
  survey_segment: string
  user_id: string
}
