# Retruco

## _Bring out shared positions from argumented statements._

**_Retruco_** is a software fostering argumentative discussion around statements and allowing to bring out shared positions.

**_Retruco_** means _immediate, precise and firm response_ in spanish.

The main repository for this project is hosted on [FramaGit](https://framagit.org/): https://framagit.org/retruco/retruco.

This repository is a Git monorepo for the following projects:

* [@retruco/core](core): Retruco library shared by API & UI
* [@retruco/api](api): Retruco web API
* [@retruco/ui](ui): Retruco web user interface
