import path from "path"

// import { validateConfig } from "./validators/config"

const config = {
  contact: {
    // email:
    name: process.env.RETRUCO_CONTACT || "Retruco-API Team",
    // url:
  },
  description:
    process.env.RETRUCO_DESCRIPTION ||
    "Bring out shared positions from argumented statements",
  email: process.env.RETRUCO_EMAIL || "retruco@localhost",
  emailSignKey: process.env.RETRUCO_EMAIL_KEY || "Retruco sign key",
  emailTemplates: path.normalize(path.join(__dirname, "..", "email-templates")),
  host: process.env.RETRUCO_HOST || "localhost",
  keys: [
    // Keys for Keygrip <https://github.com/crypto-utils/keygrip>, used by signed cookie keys, etc
    process.env.RETRUCO_KEY || "Retruco-API not very secret key, to override",
  ],
  languages: [
    "en",
    "fr",
  ],
  license: {
    // API license (not software license)
    name: "MIT",
    url: "http://opensource.org/licenses/MIT",
  },
  // matrix: {
  //   accessToken: "access_token",
  //   rejectUnauthorized: true,
  //   roomId: "!room_id:domain",
  //   serverUrl: "https://localhost:8448",
  // },
  matrix: null,
  redis: {}, // Cf https://github.com/luin/ioredis
  // title: process.env.RETRUCO_TITLE || "Retruco-API",
  title: "Retruco-API (dev)",
  twitter: {
    access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
    access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
    consumer_key: process.env.TWITTER_CONSUMER_KEY,
    consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
  },
  ui: {
    url: "http://localhost:3001",
  },
  uploads: path.normalize(path.join(__dirname, "..", "uploads")),
  wsUrl: "ws://localhost:3000",
}

// const [validConfig, error] = validateConfig(config)
// if (error !== null) {
//   console.error(
//     `Error in configuration:\n${JSON.stringify(
//       validConfig,
//       null,
//       2,
//     )}\nError:\n${JSON.stringify(error, null, 2)}`,
//   )
//   process.exit(-1)
// }

// export default validConfig
export default config