export { Step } from "@retruco/core/lib/step"
import { Step } from "@retruco/core/lib/step"

import { getUserAnswer } from "./answer"
import { Session } from "./session"

export enum StepAnsweredMode {
  NEVER,
  THREE_MONTHS_AGO,
}

/// Default function that retrieves answer for step and considers question to be answered
/// when answer exits and is not dated from more than 3 months.
export async function answerStep(
  mode: StepAnsweredMode,
  session: Session,
  step: Step,
): Promise<void> {
  if (step.question !== undefined) {
    const answer = await getUserAnswer(step.question.path, session.userId)
    if (answer !== null) {
      step.answer = answer
      switch (mode) {
        case StepAnsweredMode.NEVER:
          break
        case StepAnsweredMode.THREE_MONTHS_AGO:
          {
            const now = new Date()
            const threeMonthsAgo = new Date(
              now.getFullYear(),
              now.getMonth() - 3,
              now.getDate(),
            )
            if (answer.updated >= threeMonthsAgo) {
              step.answered = true
            }
          }
          break
      }
    }
  }
}
