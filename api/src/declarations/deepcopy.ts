declare module "deepcopy" {
  // /// <reference types="node" />

  function deepCopy(value: any, options?: any): any

  export = deepCopy
}
