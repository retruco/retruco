import { validateNonEmptyTrimmedString } from "@biryani/core"
import deepCopy from "deepcopy"
import { NextFunction, Request, Response } from "express"

import { getUserAnswer } from "../answer"
import { AskedQuestion, AskedQuestionEntry } from "../asked_question"
import { db } from "../database"
import { wrapAsyncMiddleware } from "../model"
import { Session } from "../session"
import { Survey } from "../survey"

interface SurveyRequest extends Request {
  survey: Survey
}

export const getSurvey = wrapAsyncMiddleware(async function getSurvey(
  req: SurveyRequest,
  res: Response,
) {
  const { survey } = req

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(survey.toJson(), null, 2))
})

export const getUserSurvey = wrapAsyncMiddleware(async function getUserSurvey(
  req: SurveyRequest,
  res: Response,
) {
  const { survey } = req

  const [body, error] = validateGetUserSurveyBody(req.body)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid body",
          },
        },
        null,
        2,
      ),
    )
  }

  const session = Session.fromEntry(
    await db.oneOrNone(
      `
      SELECT *
      FROM sessions
      WHERE
        token = $<sessionToken>
    `,
      body,
    ),
  )
  if (session === null) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Invalid or obsolete session token",
          },
        },
        null,
        2,
      ),
    )
  }

  const items = []
  const askedQuestions: AskedQuestion[] = (await db.any(
    `
      SELECT *
      FROM asked_questions
      WHERE
        survey_segment = $<surveySegment>
        AND user_id = $<userId>
      ORDER BY id
    `,
    {
      surveySegment: survey.segment,
      userId: session.userId,
    },
  )).map(
    (askedQuestionEntry: AskedQuestionEntry) =>
      AskedQuestion.fromEntry(askedQuestionEntry)!,
  )
  for (const askedQuestion of askedQuestions) {
    const answer = await getUserAnswer(askedQuestion.question.path, session.userId)
    items.push({
      answer: answer === null ? null : answer.toJson(),
      askedQuestionId: askedQuestion.id,
      question: askedQuestion.question.toJson(),
    })
  }

  const lastAskedQuestion = askedQuestions[askedQuestions.length - 1]
  let previousState =
    lastAskedQuestion === undefined ? undefined : lastAskedQuestion.state
  for (;;) {
    const { answer, question, state } = await survey.walkList!(
      survey,
      session,
      deepCopy(previousState),
    )
    if (question === undefined) {
      break
    }
    items.push({
      answer: answer === undefined ? null : answer.toJson(),
      question: question === undefined ? null : question.toJson(),
    })
    previousState = state
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        items,
        name: survey.name,
        segment: survey.segment,
      },
      null,
      2,
    ),
  )
})

export const listSurveys = wrapAsyncMiddleware(async function listSurveys(
  _req: Request,
  res: Response,
) {
  const surveys = Object.values(Survey.bySegment)
    .sort((a: Survey, b: Survey) => a.name.localeCompare(b.name))
    .map((survey: Survey) => survey.toJson())

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        surveys,
      },
      null,
      2,
    ),
  )
})

/// Return the surveys run directly by clients for corrent user.
export const listUserSurveys = wrapAsyncMiddleware(async function listUserSurveys(
  req: Request,
  res: Response,
) {
  const [body, error] = validateListUsersSurveyBody(req.body)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid body",
          },
        },
        null,
        2,
      ),
    )
  }

  const session = Session.fromEntry(
    await db.oneOrNone(
      `
      SELECT *
      FROM sessions
      WHERE
        token = $<sessionToken>
    `,
      body,
    ),
  )
  if (session === null) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Invalid or obsolete session token",
          },
        },
        null,
        2,
      ),
    )
  }

  const userMainSurveysSegments = (await db.any(
    `
      SELECT DISTINCT survey_segment
      FROM asked_questions
      WHERE user_id = $<userId>
    `,
    {
      userId: session.userId,
    },
  )).map(entry => entry.survey_segment)

  const userMainSurveysJson = userMainSurveysSegments
    .map(surveySegment => Survey.bySegment[surveySegment])
    .filter(survey => survey !== undefined)
    .sort((a, b) => a.name.localeCompare(b.name))
    .map(survey => survey.toJson())

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        surveys: userMainSurveysJson,
      },
      null,
      2,
    ),
  )
})

export const requireSurvey = wrapAsyncMiddleware(async function requireSurvey(
  req: SurveyRequest,
  res: Response,
  next: NextFunction,
) {
  const { surveySegment } = req.params

  const survey = Survey.bySegment[surveySegment]
  if (survey === undefined) {
    res.writeHead(404, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `No survey with segment "${surveySegment}".`,
          },
        },
        null,
        2,
      ),
    )
  }
  req.survey = survey

  return next()
})

function validateGetUserSurveyBody(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "sessionToken"
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateListUsersSurveyBody(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "sessionToken"
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
