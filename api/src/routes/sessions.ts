import { validateChoice, validateMaybeTrimmedString } from "@biryani/core"
import { Request, Response } from "express"

import { acceptedLanguages } from "../locales"
import { wrapAsyncMiddleware } from "../model"
import { createSession } from "../session"
import serverConfig from "../server_config"

const { quiz } = serverConfig

export const startQuizSession = wrapAsyncMiddleware(async function startQuizSession(
  _req: Request,
  res: Response,
) {
  const [session, sessionError] = await createSession({
    clientSegment: quiz.segment,
    clientToken: quiz.token,
    language: "fr", // TODO
  })
  if (sessionError !== null) {
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            message: sessionError,
          },
        },
        null,
        2,
      ),
    )
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify({ sessionToken: session!.token }, null, 2))
})

export const startSession = wrapAsyncMiddleware(async function startSession(
  req: Request,
  res: Response,
) {
  const [body, error] = validateStartSessionBody(req.body)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid body",
          },
        },
        null,
        2,
      ),
    )
  }

  const [session, sessionError] = await createSession(body)
  if (sessionError !== null) {
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            message: sessionError,
          },
        },
        null,
        2,
      ),
    )
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify({ sessionToken: session!.token }, null, 2))
})

function validateStartSessionBody(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  for (const key of ["clientSegment", "clientToken", "username", "userId"]) {
    remainingKeys.delete(key)
    const [value, error] = validateMaybeTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }
  if (
    errors.clientSegment === undefined &&
    data.clientSegment !== null &&
    errors.clientToken === undefined &&
    data.clientToken === null
  ) {
    errors.clientToken = "Missing value"
  }
  if (errors.clientToken === undefined) {
    if (data.clientToken === null) {
      if (errors.username === undefined && data.username !== null) {
        errors.username = "Must be empty when client is missing"
      }
    } else {
      if (errors.userId === undefined && data.userId !== null) {
        errors.userId = 'Must be empty when client is given (use "username" instead)'
      }
    }
  }

  {
    const key = "language"
    remainingKeys.delete(key)
    const [value, error] = validateChoice(acceptedLanguages)(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
