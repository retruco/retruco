import { NextFunction, Request, Response } from "express"

import { Client } from "../client"
import { db } from "../database"
import { wrapAsyncMiddleware } from "../model"

interface ClientRequest extends Request {
  client: Client
}

export const getClient = wrapAsyncMiddleware(async function getClient(
  req: ClientRequest,
  res: Response,
) {
  const { client } = req

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(client.toJson(), null, 2))
})

export const listClients = wrapAsyncMiddleware(async function listClients(
  _req: Request,
  res: Response,
) {
  const clientsJson = (await db.any(
    `
      SELECT *
      FROM clients
      ORDER BY name
    `,
  ))
    .map(Client.fromEntry)
    .map(client => (client === null ? client : client.toJson()))

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        clients: clientsJson,
      },
      null,
      2,
    ),
  )
})

export const requireClient = wrapAsyncMiddleware(async function requireClient(
  req: ClientRequest,
  res: Response,
  next: NextFunction,
) {
  const { clientSegment } = req.params

  const client = Client.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM clients
        where segment = $<clientSegment>
      `,
      {
        clientSegment,
      },
    ),
  )
  if (client === null) {
    res.writeHead(404, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 404,
            message: `No client with segment "${clientSegment}".`,
          },
        },
        null,
        2,
      ),
    )
  }
  req.client = client

  return next()
})
