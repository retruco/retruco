import { validateNonEmptyTrimmedString } from "@biryani/core"
import { Request, Response } from "express"

import { db } from "../database"
import { wrapAsyncMiddleware } from "../model"
import { Session } from "../session"

export const getSessionLanguage = wrapAsyncMiddleware(async function getSessionLanguage(
  req: Request,
  res: Response,
) {
  const [body, error] = validateGetSessionLanguageBody(req.body)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid body",
          },
        },
        null,
        2,
      ),
    )
  }

  const session = Session.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM sessions
        WHERE
          token = $<sessionToken>
      `,
      body,
    ),
  )
  if (session === null) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Invalid or obsolete session token",
          },
        },
        null,
        2,
      ),
    )
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        language: session.language,
      },
      null,
      2,
    ),
  )
})

function validateGetSessionLanguageBody(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "sessionToken"
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
