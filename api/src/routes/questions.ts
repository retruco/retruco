import { validateNumber, validateNonEmptyTrimmedString } from "@biryani/core"
import assert from "assert"
import deepCopy from "deepcopy"
import deepEqual from "deep-equal"
import { Request, Response } from "express"

import { getUserAnswer, upsertUserAnswer } from "../answer"
import { AskedQuestion } from "../asked_question"
import { db } from "../database"
import { wrapAsyncMiddleware } from "../model"
import { Session } from "../session"
import { Survey } from "../survey"

export const answerQuestion = wrapAsyncMiddleware(async function answerQuestion(
  req: Request,
  res: Response,
) {
  const [body, error] = validateAnswerQuestionBody(req.body)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid body",
          },
        },
        null,
        2,
      ),
    )
  }

  const session = Session.fromEntry(
    await db.oneOrNone(
      `
      SELECT *
      FROM sessions
      WHERE
        token = $<sessionToken>
    `,
      body,
    ),
  )
  if (session === null) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Invalid or obsolete session token",
          },
        },
        null,
        2,
      ),
    )
  }

  const askedQuestion = AskedQuestion.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM asked_questions
        WHERE
          id = $<askedQuestionId>
          AND survey_segment = $<surveySegment>
          AND user_id = $<userId>
      `,
      {
        askedQuestionId: body.askedQuestionId,
        surveySegment: body.surveySegment,
        userId: session.userId,
      },
    ),
  )
  if (askedQuestion === null) {
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            message:
              "Invalid asked question ID => Trying to answer to an unasked question",
          },
        },
        null,
        2,
      ),
    )
  }

  const [value, valueError] = askedQuestion.question.validateAnswerValueJson(body.value)
  if (valueError !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify({ value: error }, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: { value: error },
            message: "Invalid body",
          },
          value,
        },
        null,
        2,
      ),
    )
  }

  const previousAnswer = await getUserAnswer(askedQuestion.question.path, session.userId)
  const answer = await upsertUserAnswer(
    askedQuestion.question.path,
    session.userId,
    value,
  )
  if (previousAnswer === null) {
    // // Answer is added => Set all the following asked questions to unanswered.
    // await db.none(
    //   `
    //     UPDATE asked_questions
    //     SET
    //       answered = false
    //     WHERE
    //       id > $<askedQuestionId>
    //       AND survey_segment = $<surveySegment>
    //       AND user_id = $<userId>
    //   `,
    //   {
    //     askedQuestionId: body.askedQuestionId,
    //     surveySegment: body.surveySegment,
    //     userId: session.userId,
    //   },
    // )
    // Answer is added => Delete the following asked questions.
    await db.none(
      `
        DELETE FROM asked_questions
        WHERE
          id > $<askedQuestionId>
          AND survey_segment = $<surveySegment>
          AND user_id = $<userId>
      `,
      {
        askedQuestionId: body.askedQuestionId,
        surveySegment: body.surveySegment,
        userId: session.userId,
      },
    )
  } else if (!deepEqual(previousAnswer.value, value)) {
    // // Answer is changed => Set all the following asked questions to unanswered.
    // await db.none(
    //   `
    //     UPDATE asked_questions
    //     SET
    //       answered = false
    //     WHERE
    //       id > $<askedQuestionId>
    //       AND survey_segment = $<surveySegment>
    //       AND user_id = $<userId>
    //   `,
    //   {
    //     askedQuestionId: body.askedQuestionId,
    //     surveySegment: body.surveySegment,
    //     userId: session.userId,
    //   },
    // )
    // Answer is changed => Delete the following asked questions.
    await db.none(
      `
        DELETE FROM asked_questions
        WHERE
          id > $<askedQuestionId>
          AND survey_segment = $<surveySegment>
          AND user_id = $<userId>
      `,
      {
        askedQuestionId: body.askedQuestionId,
        surveySegment: body.surveySegment,
        userId: session.userId,
      },
    )
  }

  // Mark asked question as answered.
  await db.none(
    `
      UPDATE asked_questions
      SET
        answered = true
      WHERE
        id = $<askedQuestionId>
        AND survey_segment = $<surveySegment>
        AND user_id = $<userId>
    `,
    {
      askedQuestionId: body.askedQuestionId,
      surveySegment: body.surveySegment,
      userId: session.userId,
    },
  )

  await db.none(
    `
      UPDATE sessions
      SET
        expires = current_timestamp + interval '1 day'
      WHERE
        token = $<token>
    `,
    {
      token: session.token,
    },
  )

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(answer.toJson(), null, 2))
})

export const getCurrentQuestion = wrapAsyncMiddleware(async function getCurrentQuestion(
  req: Request,
  res: Response,
) {
  const [body, error] = validateGetCurrentQuestionBody(req.body)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid body",
          },
        },
        null,
        2,
      ),
    )
  }

  const session = Session.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM sessions
        WHERE
          token = $<sessionToken>
      `,
      body,
    ),
  )
  if (session === null) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Invalid or obsolete session token",
          },
        },
        null,
        2,
      ),
    )
  }

  const survey = Survey.bySegment[body.surveySegment]
  if (survey === undefined) {
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            message: "Invalid survey segment in session",
          },
        },
        null,
        2,
      ),
    )
  }

  let current
  let askedQuestion = AskedQuestion.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM asked_questions
        WHERE
          survey_segment = $<surveySegment>
          AND user_id = $<userId>
          AND answered = false
        ORDER BY id
        LIMIT 1
      `,
      {
        surveySegment: body.surveySegment,
        userId: session.userId,
      },
    ),
  )
  if (askedQuestion !== null) {
    const answer = await getUserAnswer(askedQuestion.question.path, session.userId)
    current = {
      answer: answer === null ? null : answer.toJson(),
      askedQuestionId: askedQuestion.id,
      language: session.language,
      question: askedQuestion.question.toJson(),
      survey: survey.toJson(),
    }
  } else {
    // Every existing asked question is answered. Retrieve the last answered
    // one and walk to the next (unanswered) one.
    const lastAnsweredAskedQuestion = AskedQuestion.fromEntry(
      await db.oneOrNone(
        `
          SELECT *
          FROM asked_questions
          WHERE
            survey_segment = $<surveySegment>
            AND user_id = $<userId>
            AND answered = true
          ORDER BY id DESC
          LIMIT 1
        `,
        {
          surveySegment: body.surveySegment,
          userId: session.userId,
        },
      ),
    )
    let previousState =
      lastAnsweredAskedQuestion === null || lastAnsweredAskedQuestion.state === null
        ? undefined
        : lastAnsweredAskedQuestion.state

    for (;;) {
      const { answer, answered, question, state } = await survey.walkQuiz!(
        survey,
        session,
        deepCopy(previousState),
      )
      if (question === undefined) {
        // Survey is finished.
        current = {
          answer: null,
          askedQuestionId: null,
          language: session.language,
          question: null,
          survey: survey.toJson(),
        }
        break
      }
      askedQuestion = AskedQuestion.fromEntry(
        await db.one(
          `
            INSERT INTO asked_questions (
              answered,
              question_json,
              state,
              survey_segment,
              user_id
            )
            VALUES (
              $<answered>,
              $<questionJson:json>,
              $<state:json>,
              $<surveySegment>,
              $<userId>
            )
            RETURNING *
          `,
          {
            answered,
            questionJson: question.toJson(),
            state,
            surveySegment: body.surveySegment,
            userId: session.userId,
          },
        ),
      )
      if (!answered) {
        current = {
          answer: answer === null || answer === undefined ? null : answer.toJson(),
          askedQuestionId: askedQuestion!.id,
          language: session.language,
          question: question.toJson(),
          survey: survey.toJson(),
        }
        break
      }
      previousState = state
    }
  }

  await db.none(
    `
      UPDATE sessions
      SET
        expires = current_timestamp + interval '1 day'
      WHERE
        token = $<token>
    `,
    {
      askedQuestionId: askedQuestion === null ? null : askedQuestion.id,
      token: session.token,
    },
  )

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(current, null, 2))
})

export const replaceQuestionState = wrapAsyncMiddleware(
  async function replaceQuestionState(req: Request, res: Response) {
    const [body, error] = validateReplaceQuestionStateBody(req.body)
    if (error !== null) {
      console.error(
        `Error in ${req.path}:\n${JSON.stringify(
          body,
          null,
          2,
        )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
      )
      res.writeHead(400, {
        "Content-Type": "application/json; charset=utf-8",
      })
      return res.end(
        JSON.stringify(
          {
            ...body,
            error: {
              code: 400,
              details: error,
              message: "Invalid body",
            },
          },
          null,
          2,
        ),
      )
    }

    const session = Session.fromEntry(
      await db.oneOrNone(
        `
      SELECT *
      FROM sessions
      WHERE
        token = $<sessionToken>
    `,
        body,
      ),
    )
    if (session === null) {
      res.writeHead(401, {
        "Content-Type": "application/json; charset=utf-8",
      })
      return res.end(
        JSON.stringify(
          {
            error: {
              code: 401,
              message: "Invalid or obsolete session token",
            },
          },
          null,
          2,
        ),
      )
    }

    const survey = Survey.bySegment[body.surveySegment]
    if (survey === undefined) {
      res.writeHead(400, {
        "Content-Type": "application/json; charset=utf-8",
      })
      return res.end(
        JSON.stringify(
          {
            error: {
              code: 400,
              message: "Invalid survey segment in session",
            },
          },
          null,
          2,
        ),
      )
    }

    const askedQuestion = AskedQuestion.fromEntry(
      await db.oneOrNone(
        `
        SELECT *
        FROM asked_questions
        WHERE
          id = $<askedQuestionId>
          AND survey_segment = $<surveySegment>
          AND user_id = $<userId>
      `,
        {
          askedQuestionId: body.askedQuestionId,
          surveySegment: body.surveySegment,
          userId: session.userId,
        },
      ),
    )
    if (askedQuestion === null) {
      res.writeHead(400, {
        "Content-Type": "application/json; charset=utf-8",
      })
      return res.end(
        JSON.stringify(
          {
            error: {
              code: 400,
              message:
                "Invalid asked question ID" +
                " => Trying to replace state of an unasked question",
            },
          },
          null,
          2,
        ),
      )
    }

    if (survey.replaceQuestionState === undefined) {
      console.error(
        `Error in ${req.path}:\n${JSON.stringify(
          body,
          null,
          2,
        )}\n\nError: Survey has no method to replace question state"`,
      )
      res.writeHead(400, {
        "Content-Type": "application/json; charset=utf-8",
      })
      return res.end(
        JSON.stringify(
          {
            ...body,
            error: {
              code: 400,
              message: "Survey has no method to replace question state",
            },
          },
          null,
          2,
        ),
      )
    }

    const [step, stateError] = await survey.replaceQuestionState(
      session,
      askedQuestion.state,
      body.state,
    )
    if (stateError !== null) {
      console.error(
        `Error in ${req.path}:\n${JSON.stringify(
          body,
          null,
          2,
        )}\n\nError:\n${JSON.stringify({ state: stateError }, null, 2)}`,
      )
      res.writeHead(400, {
        "Content-Type": "application/json; charset=utf-8",
      })
      return res.end(
        JSON.stringify(
          {
            ...body,
            error: {
              code: 400,
              details: { state: stateError },
              message: "Invalid body",
            },
            state: step.state,
          },
          null,
          2,
        ),
      )
    }

    const { answer, question, state } = step
    assert.strictEqual(answer, undefined)
    assert.notStrictEqual(question, undefined)

    await db.none(
      `
      UPDATE asked_questions
      SET
        answered = false,
        question_json = $<questionJson:json>,
        state = $<state:json>
      WHERE
        id = $<askedQuestionId>
        AND survey_segment = $<surveySegment>
        AND user_id = $<userId>
    `,
      {
        askedQuestionId: body.askedQuestionId,
        questionJson: question!.toJson(),
        state,
        surveySegment: body.surveySegment,
        userId: session.userId,
      },
    )

    await db.none(
      `
      UPDATE sessions
      SET
        expires = current_timestamp + interval '1 day'
      WHERE
        token = $<token>
    `,
      session,
    )

    res.writeHead(204)
    res.end()
  },
)

export const skipQuestion = wrapAsyncMiddleware(async function skipQuestion(
  req: Request,
  res: Response,
) {
  const [body, error] = validateSkipQuestionBody(req.body)
  if (error !== null) {
    console.error(
      `Error in ${req.path}:\n${JSON.stringify(
        body,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid body",
          },
        },
        null,
        2,
      ),
    )
  }

  const session = Session.fromEntry(
    await db.oneOrNone(
      `
      SELECT *
      FROM sessions
      WHERE
        token = $<sessionToken>
    `,
      body,
    ),
  )
  if (session === null) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Invalid or obsolete session token",
          },
        },
        null,
        2,
      ),
    )
  }

  const askedQuestion = AskedQuestion.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM asked_questions
        WHERE
          id = $<askedQuestionId>
          AND survey_segment = $<surveySegment>
          AND user_id = $<userId>
      `,
      {
        askedQuestionId: body.askedQuestionId,
        surveySegment: body.surveySegment,
        userId: session.userId,
      },
    ),
  )
  if (askedQuestion === null) {
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            message:
              "Invalid asked question ID => Trying to answer to an unasked question",
          },
        },
        null,
        2,
      ),
    )
  }

  // Mark asked question as answered (skipped).
  await db.none(
    `
      UPDATE asked_questions
      SET
        answered = true
      WHERE
        id = $<askedQuestionId>
        AND survey_segment = $<surveySegment>
        AND user_id = $<userId>
    `,
    {
      askedQuestionId: body.askedQuestionId,
      surveySegment: body.surveySegment,
      userId: session.userId,
    },
  )

  await db.none(
    `
      UPDATE sessions
      SET
        expires = current_timestamp + interval '1 day'
      WHERE
        token = $<token>
    `,
    {
      token: session.token,
    },
  )

  res.writeHead(204)
  res.end()
})

function validateAnswerQuestionBody(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "askedQuestionId"
    remainingKeys.delete(key)
    const [value, error] = validateNumber(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of ["sessionToken", "surveySegment"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "value"
    remainingKeys.delete(key)
    const [value, error] = [data[key], null] // Validation is done later.
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateGetCurrentQuestionBody(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  for (const key of ["sessionToken", "surveySegment"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateReplaceQuestionStateBody(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "askedQuestionId"
    remainingKeys.delete(key)
    const [value, error] = validateNumber(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of ["sessionToken", "surveySegment"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  // Note: State is validated later by `survey.replaceQuestionState()`.
  {
    const key = "state"
    remainingKeys.delete(key)
    const [value, error] = [data[key], null]
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateSkipQuestionBody(data: any) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object, got ${typeof data}`]
  }

  data = {
    ...data,
  }
  const remainingKeys = new Set(Object.keys(data))
  const errors: { [key: string]: any } = {}

  {
    const key = "askedQuestionId"
    remainingKeys.delete(key)
    const [value, error] = validateNumber(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of ["sessionToken", "surveySegment"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
