import { Session } from "@retruco/core/lib/session"
import assert from "assert"
import { randomBytes } from "crypto"

import { Client } from "./client"
import { ClientUser } from "./client_user"
import { db } from "./database"
import { ObjectEntry } from "./object"
import { User } from "./user"

export { Session } from "@retruco/core/lib/session"

export async function createSession({
  clientSegment = null,
  clientToken = null,
  language,
  userId = null,
  username = null
}: {
  clientSegment?: string | null,
  clientToken?: string | null,
  language: string,
  userId?: number | null
  username?: string | null,
}): Promise<[Session | null, string | null]> {
  let user = null
  if (userId !== null) {
    const objectEntry: ObjectEntry | null = await db.oneOrNone(
      `
        SELECT *
        FROM objects
        WHERE
          id = $<userId>
          AND type = 'User'
      `,
      { userId },
    )
    if (objectEntry === null) {
      return [null, `No user with id "${userId}".`]
    }

    user = User.fromObjectAndUserEntries(
      objectEntry,
      await db.oneOrNone(
        `
          SELECT *
          FROM users
          WHERE id = $<userId>
        `,
        { userId },
      ),
    )
    if (user === null) {
      return [null, `No user with id "${userId}".`]
    }
  }

  if (clientSegment !== null) {
    const client = Client.fromEntry(
      await db.oneOrNone(
        `
          SELECT *
          FROM clients
          where segment = $<clientSegment>
        `,
        { clientSegment },
      ),
    )
    if (client === null) {
      return [null, `No client with segment "${clientSegment}".`]
    }
    if (client.token !== clientToken) {
      return [null, `Invalid token for client with segment "${clientSegment}".`]
    }

    if (username !== null) {
      let clientUser = ClientUser.fromEntry(
        await db.oneOrNone(
          `
            SELECT *
            FROM clients_users
            WHERE
              client_segment = $<clientSegment>
              AND username = $<username>
          `,
          {
            clientSegment,
            username,
          },
        ),
      )
      if (clientUser === null) {
        const objectEntry: ObjectEntry = await db.one(
          `
            INSERT INTO objects (
              created_at,
              type
            )
            VALUES (
              current_timestamp,
              'User'
            )
            RETURNING *
          `,
        )
        user = User.fromObjectAndUserEntries(
          objectEntry,
          await db.one(
            `
              INSERT INTO users (
                id,
                is_admin
              )
              VALUES (
                $<userId>,
                false
              )
              RETURNING *
            `,
            { userId: objectEntry.id },
          ),
        )
        clientUser = ClientUser.fromEntry(
          await db.one(
            `
              INSERT INTO clients_users (
                client_segment,
                user_id,
                username
              )
              VALUES (
                $<clientSegment>,
                $<userId>,
                $<username>
              )
              RETURNING *
            `,
            {
              clientSegment: client.segment,
              userId: user!.id,
              username,
            },
          ),
        )
      }
      assert.notStrictEqual(clientUser, null)
    }
  }

  if (user === null) {
    // Create an anonymous user.
    const objectEntry: ObjectEntry = await db.one(
      `
        INSERT INTO objects (
          created_at,
          type
        )
        VALUES (
          current_timestamp,
          'User'
        )
        RETURNING *
      `,
    )
    user = User.fromObjectAndUserEntries(
      objectEntry,
      await db.one(
        `
          INSERT INTO users (
            id,
            is_admin
          )
          VALUES (
            $<userId>,
            false
          )
          RETURNING *
        `,
        { userId: objectEntry.id },
      ),
    )
  }

  let session = Session.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM sessions
        WHERE
          client_segment = $<clientSegment>
          AND user_id = $<userId>
      `,
      {
        clientSegment,
        userId: user!.id,
      },
    ),
  )
  if (session === null) {
    session = Session.fromEntry(
      await db.one(
        `
          INSERT INTO sessions (
            token,
            client_segment,
            expires,
            language,
            user_id
          )
          VALUES (
            $<token>,
            $<clientSegment>,
            current_timestamp + interval '1 day',
            $<language>,
            $<userId>
          )
          RETURNING *
        `,
        {
          clientSegment,
          language,
          token: await generateToken(),
          userId: user!.id,
        },
      ),
    )
  } else {
    session = Session.fromEntry(
      await db.one(
        `
        UPDATE sessions
        SET
          expires = current_timestamp + interval '1 day',
          language = $<language>
        WHERE
          client_segment = $<clientSegment>
          AND user_id = $<userId>
        RETURNING *
      `,
        {
          clientSegment,
          language,
          userId: user!.id,
        },
      ),
    )
  }
  assert.notStrictEqual(session, null)
  return [session, null]
}

async function generateToken() {
  return new Promise(resolve =>
    randomBytes(48, (_err, buffer) => resolve(buffer.toString("hex"))),
  )
}
