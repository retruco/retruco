// Retruco-API -- HTTP API to bring out shared positions from argumented statements
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2016, 2017 Paula Forteza & Emmanuel Raviart
// Copyright (C) 2018, 2019 Emmanuel Raviart
// https://framagit.org/retruco/retruco-api
//
// Retruco-API is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Retruco-API is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// import {schemaByPath} from "./schemas"

export const debateKeySymbols = ["con", "option", "pro", "remark", "source"]
export const idBySymbol: { [symbol: string]: string } = {}

export const symbolizedTypedValues = [
  // Basic schemas (aka types)

  // schema:object is created manually because it references itself.
  // {
  //   symbol: "schema:object",
  //   schemaSymbol: "schema:object",
  //   value: {type: "object"},
  //   widgetSymbol: null,
  // },
  {
    symbol: "schema:boolean",
    schemaSymbol: "schema:object",
    value: { type: "boolean" },
    widgetSymbol: null,
  },
  {
    symbol: "schema:booleans-array",
    schemaSymbol: "schema:object",
    value: {
      type: "array",
      items: { type: "boolean" },
    },
    widgetSymbol: null,
  },
  {
    symbol: "schema:email",
    schemaSymbol: "schema:object",
    value: { type: "string", format: "email" },
    widgetSymbol: null,
  },
  {
    symbol: "schema:emails-array",
    schemaSymbol: "schema:object",
    value: {
      type: "array",
      items: { type: "string", format: "email" },
    },
    widgetSymbol: null,
  },
  {
    symbol: "schema:number",
    schemaSymbol: "schema:object",
    value: { type: "number" },
    widgetSymbol: null,
  },
  {
    symbol: "schema:numbers-array",
    schemaSymbol: "schema:object",
    value: {
      type: "array",
      items: { type: "number" },
    },
    widgetSymbol: null,
  },
  {
    symbol: "schema:string",
    schemaSymbol: "schema:object",
    value: { type: "string" },
    widgetSymbol: null,
  },
  {
    symbol: "schema:strings-array",
    schemaSymbol: "schema:object",
    value: {
      type: "array",
      items: { type: "string" },
    },
    widgetSymbol: null,
  },
  {
    symbol: "schema:uri",
    schemaSymbol: "schema:object",
    value: { type: "string", format: "uri" },
    widgetSymbol: null,
  },
  {
    symbol: "schema:uri-reference",
    schemaSymbol: "schema:object",
    value: { type: "string", format: "uri-reference" },
    widgetSymbol: null,
  },
  {
    symbol: "schema:uri-references-array",
    schemaSymbol: "schema:object",
    value: {
      type: "array",
      items: { type: "string", format: "uri-reference" },
    },
    widgetSymbol: null,
  },
  {
    symbol: "schema:uris-array",
    schemaSymbol: "schema:object",
    value: {
      type: "array",
      items: { type: "string", format: "uri" },
    },
    widgetSymbol: null,
  },

  // More complex Schemas

  {
    symbol: "schema:bijective-card-reference",
    schemaSymbol: "schema:object",
    value: {
      $ref: "/schemas/bijective-card-reference",
    },
    widgetSymbol: null,
  },
  {
    // Import only symbol. Not used internally.
    symbol: "schema:bijective-card-references-array",
    schemaSymbol: "schema:object",
    value: {
      type: "array",
      items: {
        $ref: "/schemas/bijective-card-reference",
      },
    },
    widgetSymbol: null,
  },
  {
    symbol: "schema:card-id",
    schemaSymbol: "schema:object",
    value: {
      $ref: "/schemas/card-id",
    },
    widgetSymbol: null,
  },
  {
    symbol: "schema:card-ids-array",
    schemaSymbol: "schema:object",
    value: {
      type: "array",
      items: {
        $ref: "/schemas/card-id",
      },
    },
    widgetSymbol: null,
  },
  {
    symbol: "schema:id",
    schemaSymbol: "schema:object",
    value: {
      $ref: "/schemas/id",
    },
    widgetSymbol: null,
  },
  {
    symbol: "schema:ids-array",
    schemaSymbol: "schema:object",
    value: {
      type: "array",
      items: {
        $ref: "/schemas/id",
      },
    },
    widgetSymbol: null,
  },
  {
    symbol: "schema:localized-string",
    schemaSymbol: "schema:object",
    value: {
      $ref: "/schemas/localized-string",
    },
    widgetSymbol: null,
  },
  {
    // Import only symbol. Not used internally.
    symbol: "schema:localized-strings-array",
    schemaSymbol: "schema:object",
    value: {
      type: "array",
      items: {
        $ref: "/schemas/localized-string",
      },
    },
    widgetSymbol: null,
  },
  {
    symbol: "schema:property-id",
    schemaSymbol: "schema:object",
    value: {
      $ref: "/schemas/property-id",
    },
    widgetSymbol: null,
  },
  {
    symbol: "schema:property-ids-array",
    schemaSymbol: "schema:object",
    value: {
      type: "array",
      items: {
        $ref: "/schemas/property-id",
      },
    },
    widgetSymbol: null,
  },
  {
    symbol: "schema:value-id",
    schemaSymbol: "schema:object",
    value: {
      $ref: "/schemas/value-id",
    },
    widgetSymbol: null,
  },
  {
    symbol: "schema:value-ids-array",
    schemaSymbol: "schema:object",
    value: {
      type: "array",
      items: {
        $ref: "/schemas/value-id",
      },
    },
    widgetSymbol: null,
  },

  // Widgets

  {
    symbol: "widget:autocomplete",
    schemaSymbol: "schema:object",
    value: {
      tag: "Autocomplete",
    },
    widgetSymbol: null,
  },
  {
    symbol: "widget:image",
    schemaSymbol: "schema:object",
    value: {
      tag: "Image",
    },
    widgetSymbol: null,
  },
  {
    symbol: "widget:input-checkbox",
    schemaSymbol: "schema:object",
    value: {
      tag: "input",
      type: "checkbox",
    },
    widgetSymbol: null,
  },
  {
    symbol: "widget:input-email",
    schemaSymbol: "schema:object",
    value: {
      tag: "input",
      type: "email",
    },
    widgetSymbol: null,
  },
  {
    symbol: "widget:input-number",
    schemaSymbol: "schema:object",
    value: {
      tag: "input",
      type: "number",
    },
    widgetSymbol: null,
  },
  {
    symbol: "widget:input-text",
    schemaSymbol: "schema:object",
    value: {
      tag: "input",
      type: "text",
    },
    widgetSymbol: null,
  },
  {
    symbol: "widget:input-url",
    schemaSymbol: "schema:object",
    value: {
      tag: "input",
      type: "url",
    },
    widgetSymbol: null,
  },
  {
    symbol: "widget:rated-item-or-set",
    schemaSymbol: "schema:object",
    value: {
      tag: "RatedItemOrSet",
    },
    widgetSymbol: null,
  },
  {
    symbol: "widget:textarea",
    schemaSymbol: "schema:object",
    value: {
      tag: "textarea",
    },
    widgetSymbol: null,
  },

  // Values

  {
    symbol: "false",
    schemaSymbol: "schema:boolean",
    value: false,
    widgetSymbol: "widget:input-checkbox",
  },
  {
    symbol: "true",
    schemaSymbol: "schema:boolean",
    value: true,
    widgetSymbol: "widget:input-checkbox",
  },

  // Keys of language properties

  {
    symbol: "en",
    schemaSymbol: "schema:string",
    value: "English Localization",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:string", ["widget:input-text", "widget:textarea"]],
    ],
  },
  {
    symbol: "fr",
    schemaSymbol: "schema:string",
    value: "French Localization",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:string", ["widget:input-text", "widget:textarea"]],
    ],
  },

  // Keys of debate properties

  {
    symbol: "con", // pros & cons
    schemaSymbol: "schema:string",
    value: "Con", // Against
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:value-ids-array", ["widget:rated-item-or-set"]],
    ],
  },
  {
    symbol: "pro", // pros & cons
    schemaSymbol: "schema:string",
    value: "Pro", // For
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:value-ids-array", ["widget:rated-item-or-set"]],
    ],
  },
  {
    symbol: "option", // Alternatives for consideration to a question
    schemaSymbol: "schema:string",
    value: "Option",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:value-ids-array", ["widget:rated-item-or-set"]],
    ],
  },
  {
    symbol: "remark",
    schemaSymbol: "schema:string",
    value: "Remark",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:value-ids-array", ["widget:rated-item-or-set"]],
    ],
  },
  {
    symbol: "source", // Sources for an affirmation or an argument.
    schemaSymbol: "schema:string",
    value: "Source",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:value-ids-array", ["widget:rated-item-or-set"]],
    ],
  },

  // Keys of discussion properties

  {
    symbol: "idea",
    schemaSymbol: "schema:string",
    value: "Idea",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:value-ids-array", ["widget:rated-item-or-set"]],
    ],
  },
  {
    symbol: "intervention",
    schemaSymbol: "schema:string",
    value: "Intervention",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:value-ids-array", ["widget:rated-item-or-set"]],
    ],
  },
  {
    symbol: "question",
    schemaSymbol: "schema:string",
    value: "Question",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:value-ids-array", ["widget:rated-item-or-set"]],
    ],
  },

  // Keys of other properties

  {
    symbol: "description",
    schemaSymbol: "schema:string",
    value: "Description",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:string", ["widget:textarea", "widget:input-text"]],
    ],
  },
  {
    symbol: "discussion",
    schemaSymbol: "schema:string",
    value: "Discussion",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [["schema:id", ["widget:autocomplete"]]],
  },
  {
    symbol: "duplicate-of",
    schemaSymbol: "schema:string",
    value: "Duplicate of",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [["schema:id", ["widget:autocomplete"]]],
  },
  {
    symbol: "license",
    schemaSymbol: "schema:string",
    value: "License",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:string", ["widget:input-text", "widget:textarea"]],
    ],
  },
  {
    symbol: "location",
    schemaSymbol: "schema:string",
    value: "Location",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:string", ["widget:input-text", "widget:textarea"]],
    ],
  },
  {
    symbol: "logo",
    schemaSymbol: "schema:string",
    value: "Logo",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:uri-reference", ["widget:image", "widget:input-url"]],
    ],
  },
  {
    symbol: "name",
    schemaSymbol: "schema:string",
    value: "Name",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:string", ["widget:input-text", "widget:textarea"]],
    ],
  },
  {
    symbol: "screenshot",
    schemaSymbol: "schema:string",
    value: "Screenshot",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:uri-reference", ["widget:image", "widget:input-url"]],
    ],
  },
  {
    symbol: "source-code",
    schemaSymbol: "schema:string",
    value: "SourceCode",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [["schema:uri", ["widget:input-url"]]],
  },
  {
    symbol: "tags",
    schemaSymbol: "schema:string",
    value: "Tags",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:value-ids-array", ["widget:rated-item-or-set"]],
    ],
  },
  {
    symbol: "title",
    schemaSymbol: "schema:string",
    value: "Title",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:string", ["widget:input-text", "widget:textarea"]],
    ],
  },
  {
    symbol: "trashed",
    schemaSymbol: "schema:string",
    value: "Trash",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [["schema:boolean", ["widget:input-checkbox"]]],
  },
  {
    symbol: "twitter-name",
    schemaSymbol: "schema:string",
    value: "Twitter Name",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [["schema:string", ["widget:input-text"]]],
  },
  {
    symbol: "type",
    schemaSymbol: "schema:string",
    value: "Type",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [
      ["schema:value-ids-array", ["widget:rated-item-or-set"]],
    ],
  },
  {
    symbol: "use-cases",
    schemaSymbol: "schema:string",
    value: "Use Cases",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [["schema:ids-array", ["widget:autocomplete"]]],
  },
  {
    symbol: "used-by",
    schemaSymbol: "schema:string",
    value: "Used by",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [["schema:ids-array", ["widget:autocomplete"]]],
  },
  {
    symbol: "used-for",
    schemaSymbol: "schema:string",
    value: "Used for",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [["schema:ids-array", ["widget:autocomplete"]]],
  },
  {
    symbol: "uses",
    schemaSymbol: "schema:string",
    value: "Uses",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [["schema:ids-array", ["widget:autocomplete"]]],
  },
  {
    symbol: "website",
    schemaSymbol: "schema:string",
    value: "Website",
    widgetSymbol: "widget:input-text",
    schemasWidgetsOrder: [["schema:uri", ["widget:input-url"]]],
  },
]

export const symbols = [
  "schema:object",
  ...symbolizedTypedValues.map(infos => infos.symbol),
]

export const symbolById: { [id: string]: string } = {}

const valueBySymbol: { [symbol: string]: any} = symbolizedTypedValues.reduce(
  (
    d: { [symbol: string]: any },
    typedValue: { symbol: string; value: any },
  ) => {
    d[typedValue.symbol] = typedValue.value
    return d
  },
  {},
)

export function getIdFromIdOrSymbol(idOrSymbol: string | null) {
  if (idOrSymbol === null) return null
  if (isNaN(parseInt(idOrSymbol))) return getIdFromSymbol(idOrSymbol)
  return idOrSymbol
}

export function getIdFromSymbol(symbol: string | null): string | null {
  if (symbol === null) return null
  let valueId = idBySymbol[symbol]
  if (valueId === undefined)
    throw `Unknown symbol for getIdFromSymbol: ${symbol}`
  return valueId
}

export function getIdOrSymbolFromId(id: string | null): string | null {
  if (id === null) return null
  return symbolById[id] || id
}

export function getValueFromSymbol(symbol: string | null) {
  if (symbol === null) return null
  let value = valueBySymbol[symbol]
  if (value === undefined)
    throw `Unknown symbol for getValueFromSymbol: ${symbol}`
  return value
}
