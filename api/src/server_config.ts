require("dotenv").config()

import { validateServerConfig } from "@biryani/server_config"

const serverConfig = {
  db: {
    database: process.env.RETRUCO_DB_NAME || "retruco",
    host: process.env.RETRUCO_DB_HOST || "localhost",
    password: process.env.RETRUCO_DB_PASSWORD || "retruco", // Change it!
    port: process.env.RETRUCO_DB_PORT || 5432,
    user: process.env.RETRUCO_DB_USER || "retruco",
  },
  listen: {
    host: null, // Listen to every IPv4 addresses.
    port: null, // Listen to config.port by default
  },
  port: process.env.RETRUCO_PORT || 3000,
  quiz: {
    segment: process.env.RETRUCO_QUIZ_SEGMENT || "retruco",
    token: process.env.RETRUCO_QUIZ_TOKEN || "retruco", // Change it!
  },
  sessionSecret: "Retruco secret", // Change it!
  smtp: {
    host: process.env.SMTP_HOST || "localhost",
    port: process.env.SMTP_PORT || 25,
    secure: process.env.SMTP_SECURE || false, // Use startTLS
    // auth: {
    //   user: "username",
    //   pass: "password",
    // },
    tls: {
      rejectUnauthorized: process.env.SMTP_REJECT_UNAUTHORIZED || false, // Accept self-signed certificates.
    },
  },
}

const [validServerConfig, error] = validateServerConfig(serverConfig)
if (error !== null) {
  console.error(
    `Error in server configuration:\n${JSON.stringify(
      validServerConfig,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}

export default validServerConfig
