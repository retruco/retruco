// Retruco-API -- HTTP API to bring out shared positions from argumented statements
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2016, 2017 Paula Forteza & Emmanuel Raviart
// Copyright (C) 2018, 2019 Emmanuel Raviart
// https://framagit.org/retruco/retruco-api
//
// Retruco-API is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Retruco-API is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import activator from "activator"
import bodyParser from "body-parser"
import express from "express"
import { execute, subscribe } from "graphql"
import { graphiqlExpress, graphqlExpress } from "graphql-server-express"
import http from "http"
import nodemailer from "nodemailer"
// import path from "path"
// import quickthumb from "quickthumb"
import { SubscriptionServer } from "subscriptions-transport-ws"
const swagger = require("swagger-express-middleware")

import config from "./config"
import { checkDatabase } from "./database"
import * as ballotsController from "./routes/ballots"
import * as clientsController from "./routes/clients"
import * as cardsController from "./routes/cards"
import * as graphqlController from "./routes/graphql"
import * as languagesController from "./routes/languages"
import * as objectsController from "./routes/objects"
import * as propertiesController from "./routes/properties"
import * as questionsController from "./routes/questions"
import * as statementsController from "./routes/statements"
import * as sessionsController from "./routes/sessions"
import * as surveysController from "./routes/surveys"
import * as uploadsController from "./routes/uploads"
import * as usersController from "./routes/users"
import * as valuesController from "./routes/values"
import * as widgetsAutocompletionsController from "./routes/widgets/autocompletions"
import { schemaByPath } from "./schemas"
import serverConfig from "./server_config"
import swaggerSpecification from "./swagger"

// Load plugins.
import "@retruco/core/lib/questions/plugins"
import "./surveys/plugins"

const { NODE_ENV } = process.env
const dev = NODE_ENV === "development"

const smtpTransport = nodemailer.createTransport(serverConfig.smtp)
activator.init({
  emailProperty: "email",
  from: config.email,
  signkey: config.emailSignKey,
  templates: activator.templates.file(config.emailTemplates),
  transport: smtpTransport,
  user: usersController.activatorUser,
})

const app = express()

app.set("title", config.title)
app.set("trust proxy", dev ? false : 1) // In production, trust first proxy.

// Enable Express case-sensitive and strict options.
app.enable("case sensitive routing")
app.enable("strict routing")

if (dev) {
  // Add logging.
  const morgan = require("morgan")
  app.use(morgan("dev"))
}

app.use(bodyParser.json({ limit: "5mb" }))

// app.use(
//   "/images",
//   quickthumb.static(path.join(config.uploads, "images"), { type: "resize" }),
// )

const swaggerMiddleware = new swagger.Middleware()
swaggerMiddleware.init(swaggerSpecification, function(/* err */) {
  app.use(swaggerMiddleware.metadata())
  app.use(swaggerMiddleware.CORS())
  app.use(
    swaggerMiddleware.files({
      apiPath: "/swagger.json", // Serve the Swagger API from "/swagger.json" instead of "/api-docs/".
      rawFilesPath: false, // Disable serving the raw Swagger files.
    }),
  )
  app.use(
    swaggerMiddleware.parseRequest({
      // Configure the cookie parser to use secure cookies.
      cookie: {
        secret: config.keys[0],
        sameSite: "lax", // See https://scotthelme.co.uk/csrf-is-really-dead/
        secure: "auto",
      },
      // Don't allow JSON content over 1mb (default is 1mb).
      json: {
        limit: "1mb",
      },
    }),
  )

  // Non Swagger-based API

  // TODO: Add this APIs to OpenAPIs/Swagger.

  app.get("/clients", clientsController.listClients)
  app.get(
    "/clients/:clientSegment",
    clientsController.requireClient,
    clientsController.getClient,
  )

  app.post("/language", languagesController.getSessionLanguage)

  app.post("/questions/answer", questionsController.answerQuestion)
  app.post("/questions/current", questionsController.getCurrentQuestion)
  app.post("/questions/replace_question_state", questionsController.replaceQuestionState)
  app.post("/questions/skip", questionsController.skipQuestion)

  app.post("/sessions", sessionsController.startSession)
  app.get("/sessions/quiz", sessionsController.startQuizSession)

  app.get("/surveys", surveysController.listSurveys)
  app.post("/surveys", surveysController.listUserSurveys)
  app.get(
    "/surveys/:surveySegment",
    surveysController.requireSurvey,
    surveysController.getSurvey,
  )
  app.post(
    "/surveys/:surveySegment",
    surveysController.requireSurvey,
    surveysController.getUserSurvey,
  )

  app.get("/widgets/autocompletion", widgetsAutocompletionsController.listAutocompletions)
  app.post("/widgets/autocompletion", widgetsAutocompletionsController.addAutocompletion)

  // Non Swagger-based, GraphQL API

  app.use(
    "/graphiql",
    graphiqlExpress({
      endpointURL: "/graphql",
      subscriptionsEndpoint: `${config.wsUrl}/subscriptions`,
    }),
  )
  app.use("/graphql", graphqlExpress({ schema: graphqlController.schema }))

  //

  app.use(swaggerMiddleware.validateRequest())

  // Swagger-based API

  app.get("/", function(_req, res) {
    res.json({
      api: 1,
      title: config.title,
    })
  })

  app.get("/cards", usersController.authenticate(false), cardsController.listCards)
  app.post("/cards", usersController.authenticate(true), cardsController.createCard)
  app.get("/cards/autocomplete", cardsController.autocompleteCards)
  app.post(
    "/cards/bundle",
    usersController.authenticate(true),
    cardsController.bundleCards,
  )
  app.post(
    "/cards/easy",
    usersController.authenticate(true),
    cardsController.createCardEasy,
  )
  app.get(
    "/cards/tags-popularity",
    usersController.authenticate(false),
    cardsController.listTagsPopularity,
  )

  app.post("/login", usersController.login)

  for (let [path, schema] of Object.entries(schemaByPath)) {
    app.get(path, (_req, res) => res.json(schema))
  }

  app.get(
    "/objects/:idOrSymbol",
    usersController.authenticate(false),
    objectsController.requireObject,
    objectsController.getObject,
  )
  app.get(
    "/objects/:idOrSymbol/next-properties",
    usersController.authenticate(false),
    objectsController.requireObject,
    objectsController.nextProperties,
  )
  app.get(
    "/objects/:idOrSymbol/properties/:keyIdOrSymbol",
    usersController.authenticate(false),
    objectsController.requireObject,
    objectsController.listObjectSameKeyProperties,
  )

  app.get(
    "/properties",
    usersController.authenticate(false),
    propertiesController.listProperties,
  )
  app.post(
    "/properties",
    usersController.authenticate(true),
    propertiesController.getOrCreateProperty,
  )
  app.get(
    "/properties/keys/autocomplete",
    propertiesController.autocompletePropertiesKeys,
  )

  app.delete(
    "/statements/:idOrSymbol/rating",
    usersController.authenticate(true),
    statementsController.requireStatement,
    ballotsController.deleteBallot,
  )
  app.get(
    "/statements/:idOrSymbol/rating",
    usersController.authenticate(true),
    statementsController.requireStatement,
    ballotsController.getBallot,
  )
  app.post(
    "/statements/:idOrSymbol/rating",
    usersController.authenticate(true),
    statementsController.requireStatement,
    ballotsController.upsertBallot,
  )

  app.post(
    "/uploads/images",
    usersController.authenticate(true),
    uploadsController.uploadImage,
  )
  app.post(
    "/uploads/images/json",
    usersController.authenticate(true),
    uploadsController.uploadImageJson,
  )

  app.get("/users", usersController.listUsersUrlName)
  app.post(
    "/users",
    usersController.createUser,
    activator.createActivateNext,
    usersController.createUserAfterActivator,
  )
  // app.put("/users", usersController.updateUser)
  app.post(
    "/users/reset-password",
    usersController.resetPassword,
    activator.createPasswordResetNext,
    usersController.resetPasswordAfterActivator,
  )
  app.delete(
    "/users/:userName",
    usersController.requireUser,
    usersController.authenticate(true),
    usersController.deleteUser,
  )
  app.get(
    "/users/:userName",
    usersController.requireUser,
    usersController.authenticate(false),
    usersController.getUser,
  )
  // app.patch("/users/:userName", usersController.requireUser, usersController.patchUser)
  app.get(
    "/users/:user/activate",
    activator.completeActivateNext,
    usersController.completeActivateAfterActivator,
  )
  app.post(
    "/users/:user/reset-password",
    activator.completePasswordResetNext,
    usersController.completeResetPasswordAfterActivator,
  )
  app.get(
    "/users/:userName/send-activation",
    usersController.requireUser,
    usersController.authenticate(true),
    usersController.sendActivation,
    activator.createActivateNext,
    usersController.sendActivationAfterActivator,
  )

  app.get("/values", usersController.authenticate(false), valuesController.listValues)
  app.post("/values", usersController.authenticate(true), valuesController.createValue)
  app.get("/values/autocomplete", valuesController.autocompleteValues)
  app.post(
    "/values/existing",
    usersController.authenticate(false),
    valuesController.getExistingValue,
  )

  // app.use(function(err, req, res /* next */) {
  //   // Error handling middleware (must be last use of app)
  //   // Don't remove the next parameter above: It is needed, otherwise it is called with (req, res, next) without err.
  //   const status = err.status || 500
  //   if (status === 500) console.error(err.stack)
  //   res.status(status).json({
  //     apiVersion: "1",
  //     code: status,
  //     message: err.message || http.STATUS_CODES[status],
  //   })
  // })

  checkDatabase()
    .then(startExpress)
    .catch(error => {
      console.log(error.stack || error)
      process.exit(1)
    })
})

function startExpress() {
  const host = serverConfig.listen.host
  const port = serverConfig.listen.port || serverConfig.port
  const server = http.createServer(app)
  server.listen(port, host, () => {
    console.log(`Listening on ${host || "*"}:${port}...`)
    // Set up the WebSocket for handling GraphQL subscriptions.
    new SubscriptionServer(
      {
        execute,
        schema: graphqlController.schema,
        subscribe,
      },
      {
        server,
        path: "/subscriptions",
      },
    )
  })
  // server.timeout = 30 * 60 * 1000 // 30 minutes (in milliseconds)
}
