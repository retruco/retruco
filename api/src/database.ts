import assert from "assert"
import PgPromise from "pg-promise"

import serverConfig from "./server_config"
import { idBySymbol, symbolById, symbols } from "./symbols"

export const pgPromise = PgPromise()
export const db = pgPromise({
  database: serverConfig.db.database,
  host: serverConfig.db.host,
  password: serverConfig.db.password,
  port: serverConfig.db.port,
  user: serverConfig.db.user,
})
export let dbSharedConnectionObject: PgPromise.IConnected<unknown> | null = null

export const versionNumber = 33
export const versionTextSearchNumber = 6

export async function checkDatabase({ ignoreTextSearchVersion = false } = {}) {
  // Check that database exists.
  dbSharedConnectionObject = await db.connect()

  assert(
    await existsTable("version"),
    'Database is not initialized. Run "npm run configure" to configure it.',
  )

  let version = await db.one("SELECT * FROM version")
  assert(
    version.number <= versionNumber,
    "Database format is too recent. Upgrade Retruco software.",
  )
  assert.strictEqual(
    version.number,
    versionNumber,
    'Database must be upgraded. Run "npm run configure" to configure it.',
  )
  assert(
    version.text <= versionTextSearchNumber,
    "Text search is too recent. Upgrade Retruco-API.",
  )
  if (!ignoreTextSearchVersion) {
    assert.strictEqual(
      version.text,
      versionTextSearchNumber,
      'Text search must be upgraded. Run "npx babel-node --extensions ".ts" src/scripts/regenerate_text_search" to regenerate text search indexes.',
    )
  }

  await checkSymbols()
}

export async function checkSymbols(): Promise<void> {
  const results = await db.any(
    `
      SELECT symbols.id, symbol
      FROM objects
      INNER JOIN values ON objects.id = values.id
      INNER JOIN symbols ON objects.id = symbols.id
      WHERE symbol in ($<symbols:csv>)
    `,
    {
      symbols,
    },
  )
  for (const { id, symbol } of results) {
    idBySymbol[symbol] = id
    symbolById[id] = symbol
  }
  for (const symbol of symbols) {
    assert.notStrictEqual(
      idBySymbol[symbol],
      undefined,
      `Symbol "${symbol}" is missing. Run "npm run configure" to define it.`,
    )
  }
}

async function existsTable(tableName: string): Promise<boolean> {
  return (await db.one(
    "SELECT EXISTS(SELECT NULL FROM information_schema.tables WHERE table_name=$1)",
    [tableName],
  )).exists
}
