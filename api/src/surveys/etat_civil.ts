import { DateQuestion, EmailQuestion, TextQuestion } from "../question"
import { Session } from "../session"
import { answerStep, Step, StepAnsweredMode } from "../step"
import { Survey } from "../survey"

enum EtatCivilStateIndex {
  AFTER_FIRST_NAME,
  AFTER_LAST_NAME,
  AFTER_BIRTHDATE,
  AFTER_EMAIL,
  AFTER_WORK_PHONE,
  AFTER_MOBILE_PHONE,
  AFTER_FACEBOOK,
  AFTER_INSTAGRAM,
  AFTER_TELEGRAM,
  AFTER_TWITTER,
}

interface EtatCivilState {
  index?: EtatCivilStateIndex
}

/// Walk to next question (without answer).
function stepForward(state?: EtatCivilState): Step {
  const { index }: EtatCivilState = state === undefined ? {} : state

  switch (index) {
    case undefined:
      return new Step(new TextQuestion("prenom", "Prénom"), {
        index: EtatCivilStateIndex.AFTER_FIRST_NAME,
      })
    case EtatCivilStateIndex.AFTER_FIRST_NAME:
      return new Step(new TextQuestion("nom", "Nom"), {
        index: EtatCivilStateIndex.AFTER_LAST_NAME,
      })
    case EtatCivilStateIndex.AFTER_LAST_NAME:
      return new Step(new DateQuestion("birthdate", "Date de naissance"), {
        index: EtatCivilStateIndex.AFTER_BIRTHDATE,
      })
    case EtatCivilStateIndex.AFTER_BIRTHDATE:
      return new Step(new EmailQuestion("email", "Courriel"), {
        index: EtatCivilStateIndex.AFTER_EMAIL,
      })
    case EtatCivilStateIndex.AFTER_EMAIL:
      return new Step(
        new TextQuestion(
          "telephone_fixe_bureau",
          "Numéro de téléphone fixe à l'Assemblée",
        ),
        { index: EtatCivilStateIndex.AFTER_WORK_PHONE },
      )
    case EtatCivilStateIndex.AFTER_WORK_PHONE:
      return new Step(
        new TextQuestion("telephone_mobile", "Numéro de téléphone mobile"),
        { index: EtatCivilStateIndex.AFTER_MOBILE_PHONE },
      )
    case EtatCivilStateIndex.AFTER_MOBILE_PHONE:
      return new Step(new TextQuestion("facebook", "Identifiant Facebook"), {
        index: EtatCivilStateIndex.AFTER_FACEBOOK,
      })
    case EtatCivilStateIndex.AFTER_FACEBOOK:
      return new Step(new TextQuestion("instagram", "Identifiant Instagram"), {
        index: EtatCivilStateIndex.AFTER_INSTAGRAM,
      })
    case EtatCivilStateIndex.AFTER_INSTAGRAM:
      return new Step(new TextQuestion("telegram", "Identifiant Telegram"), {
        index: EtatCivilStateIndex.AFTER_TELEGRAM,
      })
    case EtatCivilStateIndex.AFTER_TELEGRAM:
      return new Step(new TextQuestion("twitter", "Identifiant Twitter"), {
        index: EtatCivilStateIndex.AFTER_TWITTER,
      })
    case EtatCivilStateIndex.AFTER_TWITTER:
      return new Step(null, { index: EtatCivilStateIndex.AFTER_TWITTER })
  }
}

/// Walk a single state to next question (and its answer).
async function walkListOrQuiz(
  _rootSurvey: Survey,
  session: Session,
  state?: EtatCivilState,
): Promise<Step> {
  const step = stepForward(state)
  await answerStep(StepAnsweredMode.THREE_MONTHS_AGO, session, step)
  return step
}

export default Survey.register(
  new Survey("etat_civil", "État civil", walkListOrQuiz, walkListOrQuiz),
)
