import { getUserAnswer } from "../answer"
import { TextQuestion } from "../question"
import { ChoiceItem, SingleChoice } from "../questions/single_choice"
import { Session } from "../session"
import { answerStep, Step, StepAnsweredMode } from "../step"
import { Survey } from "../survey"

enum PerceptionTaxeCarboneStateIndex {
  SEXE,
  AGE,
  EMPLOI,
  CATEGORIE_SOCIO_PROFESSIONNELLE,
  DIPLOME,
  FIN,
}

interface PerceptionTaxeCarboneState {
  index?: PerceptionTaxeCarboneStateIndex
  sexe?: "Féminin" | "Masculin" | null
}

function sexualizedChoiceItem(
  sexe: undefined | "Féminin" | "Masculin" | null,
  maleText: string,
  femaleText: string,
  otherText?: string,
): ChoiceItem {
  return {
    label: sexualizedText(sexe, maleText, femaleText, otherText),
    value: otherText === undefined ? maleText : otherText,
  }
}

function sexualizedText(
  sexe: undefined | "Féminin" | "Masculin" | null,
  maleText: string,
  femaleText: string,
  otherText?: string,
): string {
  switch (sexe) {
    case "Féminin":
      return femaleText
    case "Masculin":
      return maleText
    default:
      return otherText === undefined ? maleText : otherText
  }
}

/// Walk to next question (without answer).
async function stepForward(
  session: Session,
  state?: PerceptionTaxeCarboneState,
): Promise<Step> {
  if (state === undefined) {
    state = {}
  }
  const { index, sexe }: PerceptionTaxeCarboneState = state

  switch (index) {
    case undefined:
      return new Step(
        new TextQuestion("code_postal", "Quel est votre code postal ?"),
        {
          ...state,
          index: PerceptionTaxeCarboneStateIndex.SEXE,
        },
      )
    case PerceptionTaxeCarboneStateIndex.SEXE:
      return new Step(
        new SingleChoice(
          "sexe",
          "Quel est votre sexe (au sens de l'État civil) ?",
          ["Féminin", "Masculin"],
        ),
        {
          ...state,
          index: PerceptionTaxeCarboneStateIndex.AGE,
        },
      )
    case PerceptionTaxeCarboneStateIndex.AGE:
      // Retrieve sex from answers, just after SEXE state
      const sexeAnswer = await getUserAnswer("sexe", session.userId)
      return new Step(
        new SingleChoice("age", "Quelle est votre tranche d'âge ?", [
          "18 à 24 ans",
          "25 à 34 ans",
          "25 à 34 ans",
          "25 à 34 ans",
          "35 à 49 ans",
          "50 à 64 ans",
          "65 ans ou plus",
        ]),
        {
          ...state,
          index: PerceptionTaxeCarboneStateIndex.EMPLOI,
          sexe: sexeAnswer === null ? null : sexeAnswer.value,
        },
      )
    case PerceptionTaxeCarboneStateIndex.EMPLOI:
      return new Step(
        new SingleChoice("emploi", "Quel est votre statut d'emploi ?", [
          "fonctionnaire",
          "CDI",
          "CDD",
          "intérimaire ou contrat précaire",
          "au chômage",
          sexualizedChoiceItem(sexe, "étudiant", "étudiante"),
          sexualizedChoiceItem(sexe, "retraité", "retraitée"),
          sexualizedChoiceItem(sexe, "autre actif", "autre active"),
          sexualizedChoiceItem(sexe, "inactif", "inactif"),
        ]),
        {
          ...state,
          index: PerceptionTaxeCarboneStateIndex.CATEGORIE_SOCIO_PROFESSIONNELLE,
        },
      )
    case PerceptionTaxeCarboneStateIndex.CATEGORIE_SOCIO_PROFESSIONNELLE:
      return new Step(
        new SingleChoice(
          "categorie_socio-professionnelle",
          "Quelle est votre catégorie socio-professionnelle ?" +
            " (Rappelons que les chômeurs sont des actifs).",
          [
            sexualizedChoiceItem(sexe, "Agriculteur", "Agricultrice"),
            sexualizedChoiceItem(sexe, "Artisan, commerçant", "Artisan, commerçante"),
            "Profession libérale, cadre",
            "Profession intermédiaire",
            sexualizedChoiceItem(sexe, "Employé", "Employée"),
            sexualizedChoiceItem(sexe, "Ouvrier", "Ouvrière"),
            sexualizedChoiceItem(sexe, "Retraité", "Retraitée"),
            sexualizedChoiceItem(sexe, "Autre inactif", "Autre inactive"),
          ],
        ),
        {
          ...state,
          index: PerceptionTaxeCarboneStateIndex.DIPLOME,
        },
      )
    case PerceptionTaxeCarboneStateIndex.DIPLOME:
      return new Step(
        new SingleChoice("diplome", "Quel est votre plus haut diplôme ?", [
          "Aucun diplôme",
          "Brevet des collèges",
          "CAP ou BEP",
          "Baccalauréat",
          "Bac +2 (BTS, DUT, DEUG," + " écoles de formation sanitaires et sociales...)",
          "Bac +3 (licence...)",
          "Bac +5 ou plus (master, école d'ingénieur ou de commerce, doctorat," +
            " médecine, maîtrise, DEA, DESS...)",
        ]),
        {
          ...state,
          index: PerceptionTaxeCarboneStateIndex.FIN,
        },
      )
    case PerceptionTaxeCarboneStateIndex.FIN:
      return new Step(null, state)
  }
}

/// Walk a single state to next question (and its answer).
async function walkListOrQuiz(
  _rootSurvey: Survey,
  session: Session,
  state?: PerceptionTaxeCarboneState,
): Promise<Step> {
  const step = await stepForward(session, state)
  await answerStep(StepAnsweredMode.THREE_MONTHS_AGO, session, step)
  return step
}

export default Survey.register(
  new Survey(
    "perception_taxe_carbone",
    "Perception de la taxe carbone",
    walkListOrQuiz,
    walkListOrQuiz,
  ),
)
