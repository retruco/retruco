import { pbkdf2Sync, randomBytes } from "crypto"

import {
  deleteUserAnswer,
  getFirstAnswer,
  getUserAnswer,
  upsertUserAnswer,
} from "../answer"
import { ConfirmQuestion, EmailQuestion, PasswordQuestion } from "../question"
import { Session } from "../session"
import { answerStep, Step, StepAnsweredMode } from "../step"
import { Survey } from "../survey"

interface SignInState {
  index?: SignInStateIndex
}

enum SignInStateIndex {
  AFTER_EMAIL,
  AFTER_PASSWORD,
  END,
  SEND_EMAIL,
  SIGN_UP_PASSWORD,
  SIGN_UP_UPSERT_ACCOUNT,
}

function newEmailStep(state: SignInState) {
  return new Step(
    new EmailQuestion("signIn.email", "Email", {
      name: "email",
      required: true,
    }),
    {
      ...state,
      index: SignInStateIndex.AFTER_EMAIL,
    },
  )
}

function newPasswordStep(state: SignInState, error: string | null = null) {
  return new Step(
    new PasswordQuestion("signIn.password", "Password", {
      error,
      name: "password",
      required: true,
    }),
    {
      ...state,
      index: SignInStateIndex.AFTER_PASSWORD,
    },
  )
}

function newVerifyEmailStep(state: SignInState) {
  return new Step(
    new ConfirmQuestion(
      "signIn.verifyEmail",
      `
        There is no account with this email address yet.
        Before creating an account with this email address,
        we must first verify that you are its legitimate owner.
        Do you want to create an account with this email address?
      `,
      {
        required: true,
      },
    ),
    {
      ...state,
      index: SignInStateIndex.SEND_EMAIL,
    },
  )
}

/// Walk to next question (without answer).
async function stepForward(session: Session, state?: SignInState): Promise<Step> {
  if (state === undefined) {
    state = {}
  }
  const { index }: SignInState = state

  switch (index) {
    case undefined:
      return newEmailStep(state)

    case SignInStateIndex.AFTER_EMAIL: {
      const emailAnswer = await getUserAnswer("signIn.email", session.userId)
      if (emailAnswer === null || emailAnswer.value === null) {
        return newEmailStep(state)
      }
      // Check if this email is already used by anybody
      const accountAnswer = await getFirstAnswer(`account["${emailAnswer.value}"]`)
      if (accountAnswer === null || accountAnswer.value === null) {
        // No account with this email exists. Offer to create account.
        return newVerifyEmailStep(state)
      }
      // An account already exists. Ask for password.
      return newPasswordStep(state)
    }

    case SignInStateIndex.AFTER_PASSWORD: {
      const emailAnswer = await getUserAnswer("signIn.email", session.userId)
      if (emailAnswer === null || emailAnswer.value === null) {
        return newEmailStep(state)
      }
      const accountAnswer = await getFirstAnswer(`account["${emailAnswer.value}"]`)
      if (accountAnswer === null || accountAnswer.value === null) {
        return newVerifyEmailStep(state)
      }
      const { passwordDigest, salt } = accountAnswer.value
      if (typeof passwordDigest !== "string" || typeof salt !== "string") {
        return newVerifyEmailStep(state)
      }
      const passwordAnswer = await deleteUserAnswer(`signIn.password`, session.userId)
      if (passwordAnswer === null || passwordAnswer.value === null) {
        return newPasswordStep(state)
      }
      let newPasswordDigest = pbkdf2Sync(passwordAnswer.value, salt, 4096, 16, "sha512")
        .toString("base64")
        .replace(/=/g, "")
      if (newPasswordDigest !== passwordDigest) {
        return newPasswordStep(state, "Invalid password")
      }
      // User is authenticated.
      // TODO: change userId in session, migrate its answers, etc.
      // TODO: Propagate authentication to client.
      return new Step(null, { index: SignInStateIndex.END })
    }

    case SignInStateIndex.END:
      return new Step(null, { index: SignInStateIndex.END })

    case SignInStateIndex.SEND_EMAIL: {
      const emailAnswer = await getUserAnswer("signIn.email", session.userId)
      if (emailAnswer === null || emailAnswer.value === null) {
        return newEmailStep(state)
      }
      // TODO: Send email.
      return new Step(
        new ConfirmQuestion(
          "signIn.sendEmail",
          `An email has been sent to "${emailAnswer.value}".`,
          {
            required: true,
          },
        ),
        {
          ...state,
          index: SignInStateIndex.SIGN_UP_PASSWORD,
        },
      )
    }

    case SignInStateIndex.SIGN_UP_PASSWORD: {
      const emailAnswer = await getUserAnswer("signIn.email", session.userId)
      if (emailAnswer === null || emailAnswer.value === null) {
        return newEmailStep(state)
      }
      return new Step(
        new PasswordQuestion("signUp.password", "New Password", {
          name: "password",
          required: true,
        }),
        {
          ...state,
          index: SignInStateIndex.SIGN_UP_UPSERT_ACCOUNT,
        },
      )
    }

    case SignInStateIndex.SIGN_UP_UPSERT_ACCOUNT: {
      const emailAnswer = await getUserAnswer("signIn.email", session.userId)
      if (emailAnswer === null || emailAnswer.value === null) {
        return newEmailStep(state)
      }
      const passwordAnswer = await deleteUserAnswer("signUp.password", session.userId)
      if (passwordAnswer === null || passwordAnswer.value === null) {
        return newEmailStep(state)
      }
      // See http://security.stackexchange.com/a/27971 for explaination of digest and salt size.
      const salt = randomBytes(16)
        .toString("base64")
        .replace(/=/g, "")
      const passwordDigest = pbkdf2Sync(passwordAnswer.value, salt, 4096, 16, "sha512")
        .toString("base64")
        .replace(/=/g, "")
      await upsertUserAnswer(`account["${emailAnswer.value}"]`, session.userId, {
        passwordDigest,
        salt,
      })

      // TODO: change userId in session, migrate its answers, etc.
      // TODO: Propagate authentication to client.
      return new Step(null, { index: SignInStateIndex.END })
    }
  }
}

/// Walk a single state to next question (and its answer).
async function walkListOrQuiz(
  _rootSurvey: Survey,
  session: Session,
  state?: SignInState,
): Promise<Step> {
  const step = await stepForward(session, state)
  await answerStep(StepAnsweredMode.NEVER, session, step)
  return step
}

export default Survey.register(
  new Survey("sign_in", "Sign in / Sign up", walkListOrQuiz, walkListOrQuiz),
)
