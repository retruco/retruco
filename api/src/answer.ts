export { Answer } from "@retruco/core/lib/answer"
import { Answer } from "@retruco/core/lib/answer"

import { db } from "./database"

export async function deleteUserAnswer(
  path: string,
  userId: number,
): Promise<Answer | null> {
  return Answer.fromEntry(
    await db.oneOrNone(
      `
        DELETE FROM answers
        WHERE
          question_path = $<path>
          AND user_id = $<userId>
        RETURNING *
      `,
      {
        path,
        userId,
      },
    ),
  )
}

export async function getFirstAnswer(path: string): Promise<Answer | null> {
  return Answer.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM answers
        WHERE
          question_path = $<path>
        LIMIT 1
      `,
      {
        path,
      },
    ),
  )
}

export async function getUserAnswer(
  path: string,
  userId: number,
): Promise<Answer | null> {
  return Answer.fromEntry(
    await db.oneOrNone(
      `
        SELECT *
        FROM answers
        WHERE
          question_path = $<path>
          AND user_id = $<userId>
      `,
      {
        path,
        userId,
      },
    ),
  )
}

export async function upsertUserAnswer(
  path: string,
  userId: number,
  value: any,
  external: boolean = false,
): Promise<Answer> {
  return Answer.fromEntry(
    await db.one(
      `
        INSERT INTO answers (
          external,
          question_path,
          updated,
          user_id,
          value
        )
        VALUES (
          $<external>,
          $<path>,
          current_timestamp,
          $<userId>,
          $<value:json>
        )
        ON CONFLICT (user_id, question_path) DO UPDATE
          SET
            external = $<external>,
            updated = current_timestamp,
            value = $<value:json>
        RETURNING *
      `,
      {
        external,
        path,
        userId,
        value,
      },
    ),
  )!
}
