# Retruco API

## _HTTP API to bring out shared positions from argumented statements_

**_Retruco_** is a software fostering argumentative discussion around statements and allowing to bring out shared positions.

**_Retruco_** means _immediate, precise and firm response_ in spanish.

The main repository for this project is hosted on [FramaGit](https://framagit.org/): https://framagit.org/retruco/retruco.

## Installation

```bash
cd api/
```

Create a `.env` file to set PostgreSQL database informations and other configuration variables (you can use `example.env` as a template). Then

```bash
npm install
```

### Create the database

#### When using _Debian GNU/Linux_

As `root` user:
```bash
apt install postgresql postgresql-contrib

su - postgres
createuser -D -P -R -S retruco
  Enter password for new role: retruco
  Enter it again: retruco
createdb -E utf-8 -O retruco retruco
psql retruco
  CREATE EXTENSION IF NOT EXISTS pg_trgm;
  \q
```

### Configure the API server

```bash
npm run configure
psql -h 127.0.0.1 -U retruco -W retruco
  Password: retruco
INSERT INTO clients (segment, name, token) VALUES ('retruco', 'Retruco (localhost)', 'retruco');
INSERT INTO clients (segment, name, token) VALUES ('demo', 'Démonstration', 'demo');
exit
```

### Launch the API server

```bash
npm run start
```

### Launch the daemon that handles pending actions

In another terminal:

```bash
node process-actions.js
```
